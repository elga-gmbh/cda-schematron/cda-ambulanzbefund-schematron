<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.51
Name: Beeinträchtigungen - kodiert
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323-closed">
   <title>Beeinträchtigungen - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']])]"
         id="d42e35724-true-d4022298e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e35724-true-d4022298e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']] (rule-reference: d42e35724-true-d4022298e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '47420-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e35771-true-d4022979e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e35771-true-d4022979e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '47420-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e35771-true-d4022979e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e35821-true-d4023137e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e35821-true-d4023137e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e35821-true-d4023137e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4023015e43-true-d4023245e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023015e43-true-d4023245e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4023015e43-true-d4023245e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4023015e62-true-d4023304e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023015e62-true-d4023304e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4023015e62-true-d4023304e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4023015e105-true-d4023361e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023015e105-true-d4023361e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4023015e105-true-d4023361e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4023365e91-true-d4023395e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4023365e91-true-d4023395e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4023365e91-true-d4023395e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4023015e128-true-d4023443e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023015e128-true-d4023443e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4023015e128-true-d4023443e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4023015e144-true-d4023488e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023015e144-true-d4023488e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4023015e144-true-d4023488e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4023458e66-true-d4023551e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023458e66-true-d4023551e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4023458e66-true-d4023551e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e35828-true-d4023719e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e35828-true-d4023719e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e35828-true-d4023719e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4023596e15-true-d4023851e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023596e15-true-d4023851e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4023596e15-true-d4023851e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4023724e50-true-d4023917e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023724e50-true-d4023917e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4023724e50-true-d4023917e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4023724e120-true-d4023979e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023724e120-true-d4023979e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4023724e120-true-d4023979e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4023724e132-true-d4024001e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023724e132-true-d4024001e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4023724e132-true-d4024001e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4023989e12-true-d4024030e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023989e12-true-d4024030e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4023989e12-true-d4024030e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4023724e143-true-d4024081e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023724e143-true-d4024081e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4023724e143-true-d4024081e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4024055e58-true-d4024142e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024055e58-true-d4024142e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4024055e58-true-d4024142e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4023596e17-true-d4024219e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023596e17-true-d4024219e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4023596e17-true-d4024219e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4023596e26-true-d4024272e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023596e26-true-d4024272e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4023596e26-true-d4024272e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4023596e30-true-d4024327e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4023596e30-true-d4024327e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4023596e30-true-d4024327e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4024320e11-true-d4024354e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024320e11-true-d4024354e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4024320e11-true-d4024354e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']])]"
         id="d42e35840-true-d4024681e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e35840-true-d4024681e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']] (rule-reference: d42e35840-true-d4024681e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43'] | self::hl7:id | self::hl7:code[(@code = '284773001' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d4024385e12-true-d4025029e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024385e12-true-d4025029e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43'] | hl7:id | hl7:code[(@code = '284773001' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d4024385e12-true-d4025029e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d4024385e59-true-d4025077e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024385e59-true-d4025077e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d4024385e59-true-d4025077e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')][hl7:originalText]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d4024385e74-true-d4025091e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024385e74-true-d4025091e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d4024385e74-true-d4025091e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4024385e98-true-d4025223e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024385e98-true-d4025223e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4024385e98-true-d4025223e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4025101e38-true-d4025331e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025101e38-true-d4025331e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4025101e38-true-d4025331e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4025101e57-true-d4025390e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025101e57-true-d4025390e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4025101e57-true-d4025390e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4025101e100-true-d4025447e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025101e100-true-d4025447e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4025101e100-true-d4025447e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4025451e91-true-d4025481e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4025451e91-true-d4025481e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4025451e91-true-d4025481e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4025101e123-true-d4025529e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025101e123-true-d4025529e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4025101e123-true-d4025529e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4025101e139-true-d4025574e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025101e139-true-d4025574e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4025101e139-true-d4025574e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4025544e66-true-d4025637e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025544e66-true-d4025637e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4025544e66-true-d4025637e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4024385e101-true-d4025805e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024385e101-true-d4025805e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4024385e101-true-d4025805e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4025682e5-true-d4025937e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025682e5-true-d4025937e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4025682e5-true-d4025937e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4025810e50-true-d4026003e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025810e50-true-d4026003e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4025810e50-true-d4026003e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4025810e120-true-d4026065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025810e120-true-d4026065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4025810e120-true-d4026065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4025810e132-true-d4026087e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025810e132-true-d4026087e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4025810e132-true-d4026087e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4026075e12-true-d4026116e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026075e12-true-d4026116e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4026075e12-true-d4026116e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4025810e143-true-d4026167e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025810e143-true-d4026167e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4025810e143-true-d4026167e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4026141e58-true-d4026228e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026141e58-true-d4026228e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4026141e58-true-d4026228e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4025682e7-true-d4026305e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025682e7-true-d4026305e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4025682e7-true-d4026305e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4025682e16-true-d4026358e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025682e16-true-d4026358e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4025682e16-true-d4026358e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4025682e20-true-d4026413e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4025682e20-true-d4026413e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4025682e20-true-d4026413e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4026406e11-true-d4026440e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026406e11-true-d4026440e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4026406e11-true-d4026440e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d4024385e103-true-d4026494e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4024385e103-true-d4026494e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d4024385e103-true-d4026494e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d4026478e1-true-d4026527e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026478e1-true-d4026527e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d4026478e1-true-d4026527e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.47'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d4026547e54-true-d4026559e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d4026547e54-true-d4026559e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d4026547e54-true-d4026559e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e35856-true-d4026854e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d42e35856-true-d4026854e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e35856-true-d4026854e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d4026575e8-true-d4027167e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026575e8-true-d4027167e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d4026575e8-true-d4027167e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4026575e57-true-d4027321e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026575e57-true-d4027321e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4026575e57-true-d4027321e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4027199e42-true-d4027429e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027199e42-true-d4027429e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4027199e42-true-d4027429e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4027199e61-true-d4027488e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027199e61-true-d4027488e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4027199e61-true-d4027488e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4027199e104-true-d4027545e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027199e104-true-d4027545e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4027199e104-true-d4027545e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4027549e91-true-d4027579e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4027549e91-true-d4027579e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4027549e91-true-d4027579e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4027199e127-true-d4027627e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027199e127-true-d4027627e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4027199e127-true-d4027627e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4027199e143-true-d4027672e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027199e143-true-d4027672e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4027199e143-true-d4027672e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4027642e66-true-d4027735e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027642e66-true-d4027735e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4027642e66-true-d4027735e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4026575e63-true-d4027903e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4026575e63-true-d4027903e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4026575e63-true-d4027903e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4027780e5-true-d4028035e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027780e5-true-d4028035e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4027780e5-true-d4028035e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4027908e50-true-d4028101e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027908e50-true-d4028101e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4027908e50-true-d4028101e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4027908e120-true-d4028163e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027908e120-true-d4028163e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4027908e120-true-d4028163e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4027908e132-true-d4028185e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027908e132-true-d4028185e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4027908e132-true-d4028185e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4028173e12-true-d4028214e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4028173e12-true-d4028214e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4028173e12-true-d4028214e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4027908e143-true-d4028265e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027908e143-true-d4028265e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4027908e143-true-d4028265e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4028239e58-true-d4028326e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4028239e58-true-d4028326e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4028239e58-true-d4028326e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4027780e7-true-d4028403e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027780e7-true-d4028403e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4027780e7-true-d4028403e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4027780e16-true-d4028456e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027780e16-true-d4028456e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4027780e16-true-d4028456e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4027780e20-true-d4028511e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4027780e20-true-d4028511e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4027780e20-true-d4028511e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.51'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.8']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4028504e11-true-d4028538e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.51-2019-11-07T100323.html"
              test="not(.)">(cdagab_section_BeeintraechtigungenKodiert)/d4028504e11-true-d4028538e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4028504e11-true-d4028538e0)</assert>
   </rule>
</pattern>
