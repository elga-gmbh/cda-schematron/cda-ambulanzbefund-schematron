<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.48
Name: ELGA History of Procedures
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536-closed">
   <title>ELGA History of Procedures</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/*[not(@xsi:nil = 'true')][not(self::hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']])]"
         id="d42e73528-true-d5599726e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73528-true-d5599726e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']] (rule-reference: d42e73528-true-d5599726e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48'] | self::hl7:id | self::hl7:code[not(@nullFlavor)] | self::hl7:code[not(@nullFlavor)] | self::hl7:code[@nullFlavor='NA'] | self::hl7:code[@nullFlavor='OTH'] | self::hl7:statusCode[@code = 'completed' or @code = 'active' or @code = 'aborted' or @code = 'cancelled' or @nullFlavor] | self::hl7:effectiveTime | self::hl7:approachSiteCode | self::hl7:targetSiteCode | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e73605-true-d5600096e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73605-true-d5600096e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48'] | hl7:id | hl7:code[not(@nullFlavor)] | hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='NA'] | hl7:code[@nullFlavor='OTH'] | hl7:statusCode[@code = 'completed' or @code = 'active' or @code = 'aborted' or @code = 'cancelled' or @nullFlavor] | hl7:effectiveTime | hl7:approachSiteCode | hl7:targetSiteCode | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e73605-true-d5600096e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:qualifier[hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')]] | self::hl7:translation)]"
         id="d42e73640-true-d5600128e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73640-true-d5600128e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:qualifier[hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')]] | hl7:translation (rule-reference: d42e73640-true-d5600128e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[not(@nullFlavor)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5600132e41-true-d5600144e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5600132e41-true-d5600144e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5600132e41-true-d5600144e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[not(@nullFlavor)]/hl7:qualifier[hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')]]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code])]"
         id="d42e73663-true-d5600177e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73663-true-d5600177e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = '7' and @codeSystem = '2.16.840.1.113883.3.7.1.0')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor = doc('include/voc-1.2.40.0.34.10.176-DYNAMIC.xml')//valueSet[1]/conceptList/exception/@code] (rule-reference: d42e73663-true-d5600177e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d42e73695-true-d5600210e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73695-true-d5600210e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d42e73695-true-d5600210e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[not(@nullFlavor)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5600214e41-true-d5600226e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5600214e41-true-d5600226e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5600214e41-true-d5600226e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[@nullFlavor='NA']/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d42e73734-true-d5600247e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73734-true-d5600247e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d42e73734-true-d5600247e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[@nullFlavor='NA']/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5600251e41-true-d5600263e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5600251e41-true-d5600263e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5600251e41-true-d5600263e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[@nullFlavor='OTH']/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation[not(@nullFlavor)])]"
         id="d42e73758-true-d5600284e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73758-true-d5600284e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation[not(@nullFlavor)] (rule-reference: d42e73758-true-d5600284e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:code[@nullFlavor='OTH']/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5600288e41-true-d5600300e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5600288e41-true-d5600300e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5600288e41-true-d5600300e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d42e73818-true-d5600328e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73818-true-d5600328e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d42e73818-true-d5600328e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e73844-true-d5600483e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73844-true-d5600483e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e73844-true-d5600483e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d5600361e47-true-d5600591e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600361e47-true-d5600591e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d5600361e47-true-d5600591e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5600361e66-true-d5600650e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600361e66-true-d5600650e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5600361e66-true-d5600650e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5600361e112-true-d5600707e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600361e112-true-d5600707e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5600361e112-true-d5600707e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5600711e79-true-d5600741e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d5600711e79-true-d5600741e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5600711e79-true-d5600741e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d5600361e135-true-d5600789e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600361e135-true-d5600789e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d5600361e135-true-d5600789e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5600361e151-true-d5600834e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600361e151-true-d5600834e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5600361e151-true-d5600834e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5600804e67-true-d5600897e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600804e67-true-d5600897e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5600804e67-true-d5600897e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e73852-true-d5601065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73852-true-d5601065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e73852-true-d5601065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5600942e15-true-d5601197e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600942e15-true-d5601197e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5600942e15-true-d5601197e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5601070e50-true-d5601263e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601070e50-true-d5601263e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5601070e50-true-d5601263e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5601070e120-true-d5601325e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601070e120-true-d5601325e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5601070e120-true-d5601325e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5601070e132-true-d5601347e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601070e132-true-d5601347e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5601070e132-true-d5601347e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5601335e12-true-d5601376e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601335e12-true-d5601376e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5601335e12-true-d5601376e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5601070e143-true-d5601427e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601070e143-true-d5601427e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5601070e143-true-d5601427e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5601401e58-true-d5601488e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601401e58-true-d5601488e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5601401e58-true-d5601488e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d5600942e17-true-d5601565e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600942e17-true-d5601565e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d5600942e17-true-d5601565e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5600942e26-true-d5601618e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600942e26-true-d5601618e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5600942e26-true-d5601618e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5600942e30-true-d5601673e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5600942e30-true-d5601673e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5600942e30-true-d5601673e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5601666e8-true-d5601700e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601666e8-true-d5601700e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5601666e8-true-d5601700e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e73865-true-d5601754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d42e73865-true-d5601754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e73865-true-d5601754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d5601731e7-true-d5601787e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.html"
              test="not(.)">(ELGA_HistoryOfProcedures)/d5601731e7-true-d5601787e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d5601731e7-true-d5601787e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.48']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5601807e54-true-d5601819e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5601807e54-true-d5601819e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5601807e54-true-d5601819e0)</assert>
   </rule>
</pattern>
