<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.40
Name: Lebensstil Tabakkonsum Quantitativ Entry
Description: Entry zur quantitativen Angabe des Tabakkonsums (inhalierte Rauch-Dosis).
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427-closed">
   <title>Lebensstil Tabakkonsum Quantitativ Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']])]"
         id="d42e66832-true-d5460225e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d42e66832-true-d5460225e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']] (rule-reference: d42e66832-true-d5460225e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40'] | self::hl7:id | self::hl7:code[(@code = '229819007' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTme | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e66867-true-d5460565e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d42e66867-true-d5460565e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40'] | hl7:id | hl7:code[(@code = '229819007' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTme | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e66867-true-d5460565e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:effectiveTme/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d42e66897-true-d5460607e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d42e66897-true-d5460607e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d42e66897-true-d5460607e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e66936-true-d5460749e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d42e66936-true-d5460749e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e66936-true-d5460749e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d5460627e42-true-d5460857e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5460627e42-true-d5460857e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d5460627e42-true-d5460857e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5460627e61-true-d5460916e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5460627e61-true-d5460916e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5460627e61-true-d5460916e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5460627e104-true-d5460973e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5460627e104-true-d5460973e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5460627e104-true-d5460973e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5460977e91-true-d5461007e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d5460977e91-true-d5461007e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5460977e91-true-d5461007e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d5460627e127-true-d5461055e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5460627e127-true-d5461055e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d5460627e127-true-d5461055e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5460627e143-true-d5461100e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5460627e143-true-d5461100e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5460627e143-true-d5461100e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5461070e66-true-d5461163e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461070e66-true-d5461163e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5461070e66-true-d5461163e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e66942-true-d5461331e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d42e66942-true-d5461331e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e66942-true-d5461331e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5461208e15-true-d5461463e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461208e15-true-d5461463e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5461208e15-true-d5461463e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5461336e50-true-d5461529e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461336e50-true-d5461529e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5461336e50-true-d5461529e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5461336e120-true-d5461591e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461336e120-true-d5461591e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5461336e120-true-d5461591e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5461336e132-true-d5461613e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461336e132-true-d5461613e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5461336e132-true-d5461613e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5461601e12-true-d5461642e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461601e12-true-d5461642e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5461601e12-true-d5461642e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5461336e143-true-d5461693e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461336e143-true-d5461693e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5461336e143-true-d5461693e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5461667e58-true-d5461754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461667e58-true-d5461754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5461667e58-true-d5461754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d5461208e17-true-d5461831e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461208e17-true-d5461831e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d5461208e17-true-d5461831e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5461208e26-true-d5461884e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461208e26-true-d5461884e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5461208e26-true-d5461884e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5461208e30-true-d5461939e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461208e30-true-d5461939e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5461208e30-true-d5461939e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5461932e11-true-d5461966e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461932e11-true-d5461966e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5461932e11-true-d5461966e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e66954-true-d5462020e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d42e66954-true-d5462020e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e66954-true-d5462020e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d5461997e6-true-d5462053e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.40-2019-11-21T133427.html"
              test="not(.)">(atcdabbr_entry_LebensstilTabakkonsumQuantitativ)/d5461997e6-true-d5462053e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d5461997e6-true-d5462053e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5462073e54-true-d5462085e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5462073e54-true-d5462085e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5462073e54-true-d5462085e0)</assert>
   </rule>
</pattern>
