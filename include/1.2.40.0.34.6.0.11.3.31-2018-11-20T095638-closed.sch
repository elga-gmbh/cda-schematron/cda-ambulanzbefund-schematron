<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.31
Name: Konsultationsgrund Problem Entry
Description: Mit dieser Observation wird ein bekanntes relevantes Gesundheitsproblem des Patienten codiert dargestellt. Die Zeitspanne, in der ein Gesundheitsproblem besteht oder bestanden hat, wird hier angegeben.  
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638-closed">
   <title>Konsultationsgrund Problem Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']])]"
         id="d42e63179-true-d5418930e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63179-true-d5418930e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']] (rule-reference: d42e63179-true-d5418930e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] | self::hl7:id | self::hl7:code | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)] | self::hl7:value[@nullFlavor='OTH'] | self::hl7:value[@nullFlavor='NA'] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]])]"
         id="d42e63242-true-d5419099e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63242-true-d5419099e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5'] | hl7:id | hl7:code | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] | hl7:value[@nullFlavor='OTH'] | hl7:value[@nullFlavor='NA'] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]] (rule-reference: d42e63242-true-d5419099e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419134e54-true-d5419146e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5419134e54-true-d5419146e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419134e54-true-d5419146e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:effectiveTime[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d42e63314-true-d5419169e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63314-true-d5419169e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d42e63314-true-d5419169e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d42e63365-true-d5419197e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63365-true-d5419197e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d42e63365-true-d5419197e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:value[not(@nullFlavor)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419201e41-true-d5419213e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5419201e41-true-d5419213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419201e41-true-d5419213e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:value[@nullFlavor='OTH']/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation[not(@nullFlavor)])]"
         id="d42e63405-true-d5419239e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63405-true-d5419239e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation[not(@nullFlavor)] (rule-reference: d42e63405-true-d5419239e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:value[@nullFlavor='OTH']/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419243e41-true-d5419255e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5419243e41-true-d5419255e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419243e41-true-d5419255e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:value[@nullFlavor='NA']/*[not(@xsi:nil = 'true')][not(self::hl7:originalText)]"
         id="d42e63442-true-d5419276e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63442-true-d5419276e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText (rule-reference: d42e63442-true-d5419276e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:value[@nullFlavor='NA']/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419280e41-true-d5419292e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d5419280e41-true-d5419292e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419280e41-true-d5419292e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']])]"
         id="d42e63456-true-d5419331e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63456-true-d5419331e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']] (rule-reference: d42e63456-true-d5419331e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1'] | self::hl7:id | self::hl7:code[(@code = 'SEV' and @codeSystem = '2.16.840.1.113883.5.4')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.189-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d5419302e8-true-d5419400e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d5419302e8-true-d5419400e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1'] | hl7:id | hl7:code[(@code = 'SEV' and @codeSystem = '2.16.840.1.113883.5.4')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.189-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d5419302e8-true-d5419400e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419433e54-true-d5419445e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5419433e54-true-d5419445e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419433e54-true-d5419445e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']])]"
         id="d42e63468-true-d5419495e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63468-true-d5419495e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']] (rule-reference: d42e63468-true-d5419495e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18'] | self::hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d5419469e8-true-d5419551e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d5419469e8-true-d5419551e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18'] | hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d5419469e8-true-d5419551e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.35'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.18']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419573e54-true-d5419585e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5419573e54-true-d5419585e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419573e54-true-d5419585e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']])]"
         id="d42e63480-true-d5419635e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63480-true-d5419635e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']] (rule-reference: d42e63480-true-d5419635e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.10'] | self::hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d5419609e8-true-d5419691e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d5419609e8-true-d5419691e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.10'] | hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d5419609e8-true-d5419691e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.36'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.10']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419713e54-true-d5419725e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5419713e54-true-d5419725e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419713e54-true-d5419725e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']])]"
         id="d42e63492-true-d5419778e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d42e63492-true-d5419778e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']] (rule-reference: d42e63492-true-d5419778e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1'] | self::hl7:code[(@code = '33999-4' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.198-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d5419749e8-true-d5419842e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.31-2018-11-20T095638.html"
              test="not(.)">(elgagab_entry_KonsultationsgrundProblem)/d5419749e8-true-d5419842e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1'] | hl7:code[(@code = '33999-4' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.198-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d5419749e8-true-d5419842e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.31'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.5']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.49'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.20'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1.1']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5419870e54-true-d5419882e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5419870e54-true-d5419882e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5419870e54-true-d5419882e0)</assert>
   </rule>
</pattern>
