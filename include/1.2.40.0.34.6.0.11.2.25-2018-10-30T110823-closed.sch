<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.25
Name: Zusammenfassende Beurteilung
Description: Zusammenfassende Gesamtschau und Beurteilung der erhobenen Befunde. Eine codierte Angabe der Diagnosen ist möglich. Beispiel: „Die Zusammenschau von Anamnese und erhobenen Befunden spricht für ein inzipientes septisches Geschehen unklarer Ätiologie.“ “67781-5 "Summarization of encounter note
                Narrative", LOINC 2.16.840.1.113883.6.1 Synonyme: aktuelle Diagnose, Ergebnis, Befundinterpretation
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823-closed">
   <title>Zusammenfassende Beurteilung</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']])]"
         id="d42e33312-true-d3732149e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33312-true-d3732149e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']] (rule-reference: d42e33312-true-d3732149e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25'] | self::hl7:code[(@code = '67781-5' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]] | self::hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e33356-true-d3732754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33356-true-d3732754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25'] | hl7:code[(@code = '67781-5' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]] | hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e33356-true-d3732754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e33380-true-d3732903e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33380-true-d3732903e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e33380-true-d3732903e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3732781e45-true-d3733011e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3732781e45-true-d3733011e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3732781e45-true-d3733011e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3732781e64-true-d3733070e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3732781e64-true-d3733070e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3732781e64-true-d3733070e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3732781e110-true-d3733127e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3732781e110-true-d3733127e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3732781e110-true-d3733127e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3733131e79-true-d3733161e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3733131e79-true-d3733161e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3733131e79-true-d3733161e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3732781e133-true-d3733209e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3732781e133-true-d3733209e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3732781e133-true-d3733209e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3732781e149-true-d3733254e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3732781e149-true-d3733254e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3732781e149-true-d3733254e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3733224e67-true-d3733317e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733224e67-true-d3733317e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3733224e67-true-d3733317e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e33387-true-d3733485e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33387-true-d3733485e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e33387-true-d3733485e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3733362e15-true-d3733617e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733362e15-true-d3733617e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3733362e15-true-d3733617e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3733490e50-true-d3733683e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733490e50-true-d3733683e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3733490e50-true-d3733683e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3733490e120-true-d3733745e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733490e120-true-d3733745e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3733490e120-true-d3733745e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3733490e132-true-d3733767e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733490e132-true-d3733767e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3733490e132-true-d3733767e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3733755e12-true-d3733796e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733755e12-true-d3733796e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3733755e12-true-d3733796e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3733490e143-true-d3733847e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733490e143-true-d3733847e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3733490e143-true-d3733847e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3733821e58-true-d3733908e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733821e58-true-d3733908e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3733821e58-true-d3733908e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3733362e17-true-d3733985e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733362e17-true-d3733985e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3733362e17-true-d3733985e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3733362e26-true-d3734038e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733362e26-true-d3734038e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3733362e26-true-d3734038e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3733362e30-true-d3734093e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3733362e30-true-d3734093e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3733362e30-true-d3734093e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3734086e8-true-d3734120e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734086e8-true-d3734120e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3734086e8-true-d3734120e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']])]"
         id="d42e33399-true-d3734166e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33399-true-d3734166e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']] (rule-reference: d42e33399-true-d3734166e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.1.3.1'] | self::hl7:value[not(@nullFlavor)])]"
         id="d3734155e1-true-d3734188e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734155e1-true-d3734188e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.1.3.1'] | hl7:value[not(@nullFlavor)] (rule-reference: d3734155e1-true-d3734188e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']])]"
         id="d42e33401-true-d3734311e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33401-true-d3734311e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']] (rule-reference: d42e33401-true-d3734311e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.13.3.6'] | self::hl7:id | self::hl7:code | self::hl7:statusCode[@code = 'active' or @code = 'completed'] | self::hl7:effectiveTime | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]] | self::hl7:reference[hl7:externalDocument])]"
         id="d3734239e1-true-d3734465e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734239e1-true-d3734465e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.13.3.6'] | hl7:id | hl7:code | hl7:statusCode[@code = 'active' or @code = 'completed'] | hl7:effectiveTime | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]] | hl7:reference[hl7:externalDocument] (rule-reference: d3734239e1-true-d3734465e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d3734239e42-true-d3734507e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734239e42-true-d3734507e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d3734239e42-true-d3734507e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:assignedAuthor[hl7:representedOrganization])]"
         id="d3734239e69-true-d3734548e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734239e69-true-d3734548e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:assignedAuthor[hl7:representedOrganization] (rule-reference: d3734239e69-true-d3734548e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization[not(@nullFlavor)])]"
         id="d3734522e8-true-d3734602e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734522e8-true-d3734602e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization[not(@nullFlavor)] (rule-reference: d3734522e8-true-d3734602e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3734522e26-true-d3734640e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734522e26-true-d3734640e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3734522e26-true-d3734640e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d3734522e41-true-d3734666e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734522e41-true-d3734666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName | hl7:softwareName (rule-reference: d3734522e41-true-d3734666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d3734522e68-true-d3734705e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734522e68-true-d3734705e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d3734522e68-true-d3734705e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d3734239e75-true-d3734754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734239e75-true-d3734754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d3734239e75-true-d3734754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:representedOrganisation)]"
         id="d3734730e12-true-d3734807e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734730e12-true-d3734807e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:representedOrganisation (rule-reference: d3734730e12-true-d3734807e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3734730e31-true-d3734845e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734730e31-true-d3734845e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3734730e31-true-d3734845e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganisation/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d3734730e36-true-d3734873e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734730e36-true-d3734873e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d3734730e36-true-d3734873e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:relatedPerson)]"
         id="d3734730e42-true-d3734932e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734730e42-true-d3734932e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:addr | hl7:telecom | hl7:relatedPerson (rule-reference: d3734730e42-true-d3734932e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3734730e55-true-d3734965e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734730e55-true-d3734965e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3734730e55-true-d3734965e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']])]"
         id="d3734239e84-true-d3735024e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734239e84-true-d3735024e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']] (rule-reference: d3734239e84-true-d3735024e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.13.3.7'] | self::hl7:id | self::hl7:code | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)] | self::hl7:value[@nullFlavor='NI'] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]])]"
         id="d3734977e7-true-d3735123e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e7-true-d3735123e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.13.3.7'] | hl7:id | hl7:code | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] | hl7:value[@nullFlavor='NI'] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]] (rule-reference: d3734977e7-true-d3735123e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d3734977e48-true-d3735167e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e48-true-d3735167e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d3734977e48-true-d3735167e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d3734977e113-true-d3735192e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e113-true-d3735192e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d3734977e113-true-d3735192e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3734977e151-true-d3735206e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e151-true-d3735206e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3734977e151-true-d3735206e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d3734977e174-true-d3735231e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e174-true-d3735231e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d3734977e174-true-d3735231e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3734977e188-true-d3735245e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e188-true-d3735245e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3734977e188-true-d3735245e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)])]"
         id="d3734977e210-true-d3735267e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e210-true-d3735267e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] (rule-reference: d3734977e210-true-d3735267e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3734977e223-true-d3735281e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e223-true-d3735281e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3734977e223-true-d3735281e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']])]"
         id="d3734977e234-true-d3735306e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e234-true-d3735306e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']] (rule-reference: d3734977e234-true-d3735306e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.13.3.21'] | self::hl7:id | self::hl7:code | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.189-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor])]"
         id="d3735291e7-true-d3735355e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735291e7-true-d3735355e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.13.3.21'] | hl7:id | hl7:code | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.189-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] (rule-reference: d3735291e7-true-d3735355e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3735291e29-true-d3735386e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735291e29-true-d3735386e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3735291e29-true-d3735386e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']])]"
         id="d3734977e244-true-d3735425e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e244-true-d3735425e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']] (rule-reference: d3734977e244-true-d3735425e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.13.3.18'] | self::hl7:code | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d3735410e7-true-d3735469e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735410e7-true-d3735469e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.13.3.18'] | hl7:code | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d3735410e7-true-d3735469e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3735410e23-true-d3735495e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735410e23-true-d3735495e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3735410e23-true-d3735495e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']])]"
         id="d3734977e254-true-d3735534e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e254-true-d3735534e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']] (rule-reference: d3734977e254-true-d3735534e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.13.3.19'] | self::hl7:code | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d3735519e7-true-d3735578e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735519e7-true-d3735578e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.13.3.19'] | hl7:code | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d3735519e7-true-d3735578e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3735519e26-true-d3735604e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735519e26-true-d3735604e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3735519e26-true-d3735604e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']])]"
         id="d3734977e265-true-d3735643e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734977e265-true-d3735643e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']] (rule-reference: d3734977e265-true-d3735643e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.11.13.3.13'] | self::hl7:code | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.198-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor])]"
         id="d3735628e7-true-d3735687e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735628e7-true-d3735687e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.11.13.3.13'] | hl7:code | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.198-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] (rule-reference: d3735628e7-true-d3735687e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3735628e26-true-d3735713e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735628e26-true-d3735713e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3735628e26-true-d3735713e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:reference[hl7:externalDocument]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument)]"
         id="d3734239e94-true-d3735749e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3734239e94-true-d3735749e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument (rule-reference: d3734239e94-true-d3735749e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.11.13.3.6']]/hl7:reference[hl7:externalDocument]/hl7:externalDocument/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text)]"
         id="d3735737e5-true-d3735771e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735737e5-true-d3735771e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:code | hl7:text (rule-reference: d3735737e5-true-d3735771e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e33403-true-d3736070e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d42e33403-true-d3736070e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e33403-true-d3736070e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d3735791e7-true-d3736383e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735791e7-true-d3736383e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d3735791e7-true-d3736383e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d3735791e58-true-d3736537e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735791e58-true-d3736537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d3735791e58-true-d3736537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3736415e45-true-d3736645e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736415e45-true-d3736645e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3736415e45-true-d3736645e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3736415e64-true-d3736704e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736415e64-true-d3736704e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3736415e64-true-d3736704e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3736415e110-true-d3736761e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736415e110-true-d3736761e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3736415e110-true-d3736761e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3736765e79-true-d3736795e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3736765e79-true-d3736795e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3736765e79-true-d3736795e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3736415e133-true-d3736843e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736415e133-true-d3736843e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3736415e133-true-d3736843e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3736415e149-true-d3736888e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736415e149-true-d3736888e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3736415e149-true-d3736888e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3736858e67-true-d3736951e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736858e67-true-d3736951e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3736858e67-true-d3736951e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d3735791e64-true-d3737119e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3735791e64-true-d3737119e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d3735791e64-true-d3737119e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3736996e5-true-d3737251e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736996e5-true-d3737251e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3736996e5-true-d3737251e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3737124e50-true-d3737317e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737124e50-true-d3737317e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3737124e50-true-d3737317e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3737124e120-true-d3737379e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737124e120-true-d3737379e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3737124e120-true-d3737379e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3737124e132-true-d3737401e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737124e132-true-d3737401e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3737124e132-true-d3737401e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3737389e12-true-d3737430e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737389e12-true-d3737430e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3737389e12-true-d3737430e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3737124e143-true-d3737481e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737124e143-true-d3737481e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3737124e143-true-d3737481e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3737455e58-true-d3737542e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737455e58-true-d3737542e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3737455e58-true-d3737542e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3736996e7-true-d3737619e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736996e7-true-d3737619e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3736996e7-true-d3737619e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3736996e16-true-d3737672e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736996e16-true-d3737672e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3736996e16-true-d3737672e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3736996e20-true-d3737727e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3736996e20-true-d3737727e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3736996e20-true-d3737727e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.25']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3737720e8-true-d3737754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.6.0.11.2.25-2018-10-30T110823.html"
              test="not(.)">(elgagab_section_ZusammenfassendeBeurteilung)/d3737720e8-true-d3737754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3737720e8-true-d3737754e0)</assert>
   </rule>
</pattern>
