<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.9.46
Name: Participant Body - Authorized Editor
Description: Enthält die OID des GDA, der einen Eintrag (entry) bearbeiten darf. Das Berechtigungssystem erlaubt die Korrektur eines Eintrags nur einem GDA mit derselben OID oder GDAs mit der Rolle "Korrekturberechtigte Person".
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540">
   <title>Participant Body - Authorized Editor</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.46
Context: *[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]
Item: (atcdabbr_other_ParticipantBodyAuthorizedEditor)
-->

   <rule fpi="RULC-1"
         context="*[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]"
         id="d42e87476-false-d5748441e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']) &gt;= 1">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']) &lt;= 1">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="count(hl7:participantRole[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Element hl7:participantRole[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="count(hl7:participantRole[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Element hl7:participantRole[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.46
Context: *[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']
Item: (atcdabbr_other_ParticipantBodyAuthorizedEditor)
-->

   <rule fpi="RULC-1"
         context="*[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']"
         id="d42e87498-false-d5748466e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.9.46')">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Der Wert von root MUSS '1.2.40.0.34.6.0.11.9.46' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.46
Context: *[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]
Item: (atcdabbr_other_ParticipantBodyAuthorizedEditor)
-->

   <rule fpi="RULC-1"
         context="*[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]"
         id="d42e87507-false-d5748480e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="string(@classCode) = ('ROL') or not(@classCode)">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Der Wert von classCode MUSS 'ROL' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="count(hl7:id[not(@nullFlavor)][not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Element hl7:id[not(@nullFlavor)][not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="count(hl7:id[not(@nullFlavor)][not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Element hl7:id[not(@nullFlavor)][not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.46
Context: *[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]/hl7:id[not(@nullFlavor)][not(@nullFlavor)]
Item: (atcdabbr_other_ParticipantBodyAuthorizedEditor)
-->

   <rule fpi="RULC-1"
         context="*[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]/hl7:id[not(@nullFlavor)][not(@nullFlavor)]"
         id="d42e87511-false-d5748496e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="not(@root) or (string-length(@root) &gt; 0 and not(matches(@root,'\s')))">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Attribute @root MUSS vom Datentyp 'cs' sein  - '<value-of select="@root"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_other_ParticipantBodyAuthorizedEditor): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
</pattern>
