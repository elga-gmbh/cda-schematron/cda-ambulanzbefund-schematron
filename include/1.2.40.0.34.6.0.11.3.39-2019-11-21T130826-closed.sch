<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.39
Name: Medical Device Entry
Description: Dieses Entry beschreibt das medizinisches Gerät oder Implantat, das vom Patienten zur Zeit verwendet wird.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826-closed">
   <title>Medical Device Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']])]"
         id="d42e66254-true-d5443804e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66254-true-d5443804e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']] (rule-reference: d42e66254-true-d5443804e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | self::hl7:id | self::hl7:text | self::hl7:effectiveTime | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e66322-true-d5444143e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66322-true-d5444143e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | hl7:id | hl7:text | hl7:effectiveTime | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e66322-true-d5444143e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e66345-true-d5444174e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66345-true-d5444174e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e66345-true-d5444174e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low)]"
         id="d42e66360-true-d5444193e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66360-true-d5444193e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low (rule-reference: d42e66360-true-d5444193e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e66382-true-d5444325e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66382-true-d5444325e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e66382-true-d5444325e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d5444203e43-true-d5444433e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444203e43-true-d5444433e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d5444203e43-true-d5444433e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5444203e62-true-d5444492e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444203e62-true-d5444492e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5444203e62-true-d5444492e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5444203e105-true-d5444549e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444203e105-true-d5444549e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5444203e105-true-d5444549e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5444553e91-true-d5444583e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d5444553e91-true-d5444583e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5444553e91-true-d5444583e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d5444203e128-true-d5444631e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444203e128-true-d5444631e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d5444203e128-true-d5444631e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5444203e144-true-d5444676e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444203e144-true-d5444676e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5444203e144-true-d5444676e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5444646e66-true-d5444739e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444646e66-true-d5444739e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5444646e66-true-d5444739e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e66389-true-d5444907e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66389-true-d5444907e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e66389-true-d5444907e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5444784e15-true-d5445039e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444784e15-true-d5445039e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5444784e15-true-d5445039e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5444912e50-true-d5445105e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444912e50-true-d5445105e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5444912e50-true-d5445105e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5444912e120-true-d5445167e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444912e120-true-d5445167e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5444912e120-true-d5445167e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5444912e132-true-d5445189e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444912e132-true-d5445189e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5444912e132-true-d5445189e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5445177e12-true-d5445218e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5445177e12-true-d5445218e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5445177e12-true-d5445218e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5444912e143-true-d5445269e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444912e143-true-d5445269e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5444912e143-true-d5445269e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5445243e58-true-d5445330e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5445243e58-true-d5445330e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5445243e58-true-d5445330e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d5444784e17-true-d5445407e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444784e17-true-d5445407e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d5444784e17-true-d5445407e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5444784e26-true-d5445460e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444784e26-true-d5445460e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5444784e26-true-d5445460e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5444784e30-true-d5445515e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5444784e30-true-d5445515e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5444784e30-true-d5445515e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5445508e11-true-d5445542e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5445508e11-true-d5445542e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5445508e11-true-d5445542e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole)]"
         id="d42e66401-true-d5445592e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66401-true-d5445592e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole (rule-reference: d42e66401-true-d5445592e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:playingDevice)]"
         id="d42e66408-true-d5445626e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66408-true-d5445626e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:playingDevice (rule-reference: d42e66408-true-d5445626e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code)]"
         id="d42e66424-true-d5445660e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66424-true-d5445660e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code (rule-reference: d42e66424-true-d5445660e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d42e66430-true-d5445684e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66430-true-d5445684e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d42e66430-true-d5445684e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d42e66530-true-d5445698e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66530-true-d5445698e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d42e66530-true-d5445698e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e66555-true-d5445736e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e66555-true-d5445736e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e66555-true-d5445736e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d5445713e5-true-d5445769e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d5445713e5-true-d5445769e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d5445713e5-true-d5445769e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5445789e54-true-d5445801e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5445789e54-true-d5445801e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5445789e54-true-d5445801e0)</assert>
   </rule>
</pattern>
