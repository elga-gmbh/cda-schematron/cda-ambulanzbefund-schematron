<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.42
Name: Lebensstil Alkoholkonsum Quantitativ Entry
Description: Entry zur quantitativen Angabe des Alkoholkonsums in Gläsern pro Tag an Tagen, an denen getrunken wird
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654-closed">
   <title>Lebensstil Alkoholkonsum Quantitativ Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']])]"
         id="d42e67499-true-d5480896e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d42e67499-true-d5480896e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']] (rule-reference: d42e67499-true-d5480896e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42'] | self::hl7:id | self::hl7:code[(@code = '443315005' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed' or @nullFlavor] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e67535-true-d5481236e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d42e67535-true-d5481236e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42'] | hl7:id | hl7:code[(@code = '443315005' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed' or @nullFlavor] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e67535-true-d5481236e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d42e67568-true-d5481278e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d42e67568-true-d5481278e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d42e67568-true-d5481278e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e67594-true-d5481420e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d42e67594-true-d5481420e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e67594-true-d5481420e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d5481298e43-true-d5481528e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481298e43-true-d5481528e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d5481298e43-true-d5481528e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5481298e62-true-d5481587e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481298e62-true-d5481587e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5481298e62-true-d5481587e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5481298e105-true-d5481644e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481298e105-true-d5481644e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5481298e105-true-d5481644e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5481648e91-true-d5481678e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d5481648e91-true-d5481678e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5481648e91-true-d5481678e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d5481298e128-true-d5481726e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481298e128-true-d5481726e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d5481298e128-true-d5481726e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5481298e144-true-d5481771e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481298e144-true-d5481771e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5481298e144-true-d5481771e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5481741e66-true-d5481834e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481741e66-true-d5481834e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5481741e66-true-d5481834e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e67601-true-d5482002e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d42e67601-true-d5482002e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e67601-true-d5482002e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5481879e15-true-d5482134e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481879e15-true-d5482134e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5481879e15-true-d5482134e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5482007e50-true-d5482200e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482007e50-true-d5482200e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5482007e50-true-d5482200e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5482007e120-true-d5482262e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482007e120-true-d5482262e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5482007e120-true-d5482262e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5482007e132-true-d5482284e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482007e132-true-d5482284e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5482007e132-true-d5482284e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5482272e12-true-d5482313e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482272e12-true-d5482313e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5482272e12-true-d5482313e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5482007e143-true-d5482364e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482007e143-true-d5482364e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5482007e143-true-d5482364e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5482338e58-true-d5482425e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482338e58-true-d5482425e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5482338e58-true-d5482425e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d5481879e17-true-d5482502e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481879e17-true-d5482502e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d5481879e17-true-d5482502e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5481879e26-true-d5482555e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481879e26-true-d5482555e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5481879e26-true-d5482555e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5481879e30-true-d5482610e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5481879e30-true-d5482610e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5481879e30-true-d5482610e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5482603e11-true-d5482637e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482603e11-true-d5482637e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5482603e11-true-d5482637e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e67613-true-d5482691e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d42e67613-true-d5482691e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e67613-true-d5482691e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d5482668e6-true-d5482724e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.42-2020-10-13T112654.html"
              test="not(.)">(atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ)/d5482668e6-true-d5482724e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d5482668e6-true-d5482724e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5482744e54-true-d5482756e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d5482744e54-true-d5482756e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5482744e54-true-d5482756e0)</assert>
   </rule>
</pattern>
