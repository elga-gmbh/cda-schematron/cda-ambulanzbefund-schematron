<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.55
Name: Lebensstil - kodiert
Description: Diese Sektion dient der
                    Erfassung von Lebensstil-Faktoren, wie Alkoholkonsum oder Rauchen.  Bei der Angabe von Werten, auf einen Abusus hindeuten, soll auf Konsistenz mit den Angaben in den Diagnosen geachtet werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947-closed">
   <title>Lebensstil - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']])]"
         id="d42e36647-true-d4081252e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36647-true-d4081252e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']] (rule-reference: d42e36647-true-d4081252e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '29762-2' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e36717-true-d4082492e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36717-true-d4082492e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '29762-2' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e36717-true-d4082492e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e36763-true-d4082650e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36763-true-d4082650e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e36763-true-d4082650e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4082528e43-true-d4082758e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4082528e43-true-d4082758e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4082528e43-true-d4082758e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4082528e62-true-d4082817e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4082528e62-true-d4082817e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4082528e62-true-d4082817e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4082528e105-true-d4082874e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4082528e105-true-d4082874e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4082528e105-true-d4082874e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4082878e91-true-d4082908e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4082878e91-true-d4082908e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4082878e91-true-d4082908e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4082528e128-true-d4082956e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4082528e128-true-d4082956e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4082528e128-true-d4082956e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4082528e144-true-d4083001e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4082528e144-true-d4083001e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4082528e144-true-d4083001e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4082971e66-true-d4083064e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4082971e66-true-d4083064e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4082971e66-true-d4083064e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e36770-true-d4083232e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36770-true-d4083232e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e36770-true-d4083232e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4083109e15-true-d4083364e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083109e15-true-d4083364e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4083109e15-true-d4083364e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4083237e50-true-d4083430e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083237e50-true-d4083430e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4083237e50-true-d4083430e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4083237e120-true-d4083492e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083237e120-true-d4083492e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4083237e120-true-d4083492e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4083237e132-true-d4083514e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083237e132-true-d4083514e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4083237e132-true-d4083514e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4083502e12-true-d4083543e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083502e12-true-d4083543e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4083502e12-true-d4083543e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4083237e143-true-d4083594e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083237e143-true-d4083594e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4083237e143-true-d4083594e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4083568e58-true-d4083655e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083568e58-true-d4083655e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4083568e58-true-d4083655e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4083109e17-true-d4083732e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083109e17-true-d4083732e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4083109e17-true-d4083732e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4083109e26-true-d4083785e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083109e26-true-d4083785e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4083109e26-true-d4083785e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4083109e30-true-d4083840e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083109e30-true-d4083840e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4083109e30-true-d4083840e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4083833e11-true-d4083867e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083833e11-true-d4083867e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4083833e11-true-d4083867e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']])]"
         id="d42e36782-true-d4084191e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36782-true-d4084191e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']] (rule-reference: d42e36782-true-d4084191e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41'] | self::hl7:id | self::hl7:code[(@code = '230056004' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.204-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d4083898e12-true-d4084537e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083898e12-true-d4084537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41'] | hl7:id | hl7:code[(@code = '230056004' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.204-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d4083898e12-true-d4084537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d4083898e45-true-d4084579e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083898e45-true-d4084579e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d4083898e45-true-d4084579e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4083898e84-true-d4084724e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083898e84-true-d4084724e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4083898e84-true-d4084724e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4084602e42-true-d4084832e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4084602e42-true-d4084832e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4084602e42-true-d4084832e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4084602e61-true-d4084891e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4084602e61-true-d4084891e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4084602e61-true-d4084891e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4084602e104-true-d4084948e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4084602e104-true-d4084948e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4084602e104-true-d4084948e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4084952e91-true-d4084982e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4084952e91-true-d4084982e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4084952e91-true-d4084982e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4084602e127-true-d4085030e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4084602e127-true-d4085030e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4084602e127-true-d4085030e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4084602e143-true-d4085075e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4084602e143-true-d4085075e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4084602e143-true-d4085075e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4085045e66-true-d4085138e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085045e66-true-d4085138e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4085045e66-true-d4085138e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4083898e90-true-d4085306e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083898e90-true-d4085306e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4083898e90-true-d4085306e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4085183e15-true-d4085438e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085183e15-true-d4085438e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4085183e15-true-d4085438e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4085311e50-true-d4085504e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085311e50-true-d4085504e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4085311e50-true-d4085504e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4085311e120-true-d4085566e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085311e120-true-d4085566e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4085311e120-true-d4085566e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4085311e132-true-d4085588e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085311e132-true-d4085588e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4085311e132-true-d4085588e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4085576e12-true-d4085617e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085576e12-true-d4085617e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4085576e12-true-d4085617e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4085311e143-true-d4085668e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085311e143-true-d4085668e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4085311e143-true-d4085668e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4085642e58-true-d4085729e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085642e58-true-d4085729e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4085642e58-true-d4085729e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4085183e17-true-d4085806e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085183e17-true-d4085806e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4085183e17-true-d4085806e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4085183e26-true-d4085859e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085183e26-true-d4085859e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4085183e26-true-d4085859e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4085183e30-true-d4085914e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085183e30-true-d4085914e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4085183e30-true-d4085914e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4085907e11-true-d4085941e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085907e11-true-d4085941e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4085907e11-true-d4085941e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d4083898e102-true-d4085995e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4083898e102-true-d4085995e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d4083898e102-true-d4085995e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d4085972e6-true-d4086028e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4085972e6-true-d4086028e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d4085972e6-true-d4086028e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d4086048e54-true-d4086060e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d4086048e54-true-d4086060e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d4086048e54-true-d4086060e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']])]"
         id="d42e36798-true-d4086369e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36798-true-d4086369e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']] (rule-reference: d42e36798-true-d4086369e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40'] | self::hl7:id | self::hl7:code[(@code = '229819007' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d4086076e14-true-d4086709e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086076e14-true-d4086709e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40'] | hl7:id | hl7:code[(@code = '229819007' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d4086076e14-true-d4086709e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d4086076e44-true-d4086751e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086076e44-true-d4086751e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d4086076e44-true-d4086751e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4086076e83-true-d4086893e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086076e83-true-d4086893e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4086076e83-true-d4086893e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4086771e42-true-d4087001e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086771e42-true-d4087001e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4086771e42-true-d4087001e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4086771e61-true-d4087060e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086771e61-true-d4087060e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4086771e61-true-d4087060e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4086771e104-true-d4087117e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086771e104-true-d4087117e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4086771e104-true-d4087117e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4087121e91-true-d4087151e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4087121e91-true-d4087151e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4087121e91-true-d4087151e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4086771e127-true-d4087199e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086771e127-true-d4087199e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4086771e127-true-d4087199e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4086771e143-true-d4087244e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086771e143-true-d4087244e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4086771e143-true-d4087244e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4087214e66-true-d4087307e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087214e66-true-d4087307e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4087214e66-true-d4087307e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4086076e89-true-d4087475e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086076e89-true-d4087475e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4086076e89-true-d4087475e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4087352e15-true-d4087607e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087352e15-true-d4087607e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4087352e15-true-d4087607e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4087480e50-true-d4087673e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087480e50-true-d4087673e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4087480e50-true-d4087673e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4087480e120-true-d4087735e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087480e120-true-d4087735e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4087480e120-true-d4087735e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4087480e132-true-d4087757e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087480e132-true-d4087757e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4087480e132-true-d4087757e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4087745e12-true-d4087786e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087745e12-true-d4087786e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4087745e12-true-d4087786e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4087480e143-true-d4087837e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087480e143-true-d4087837e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4087480e143-true-d4087837e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4087811e58-true-d4087898e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087811e58-true-d4087898e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4087811e58-true-d4087898e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4087352e17-true-d4087975e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087352e17-true-d4087975e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4087352e17-true-d4087975e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4087352e26-true-d4088028e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087352e26-true-d4088028e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4087352e26-true-d4088028e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4087352e30-true-d4088083e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4087352e30-true-d4088083e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4087352e30-true-d4088083e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4088076e11-true-d4088110e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088076e11-true-d4088110e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4088076e11-true-d4088110e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d4086076e101-true-d4088164e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4086076e101-true-d4088164e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d4086076e101-true-d4088164e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d4088141e6-true-d4088197e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088141e6-true-d4088197e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d4088141e6-true-d4088197e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d4088217e54-true-d4088229e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d4088217e54-true-d4088229e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d4088217e54-true-d4088229e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']])]"
         id="d42e36815-true-d4088538e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36815-true-d4088538e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']] (rule-reference: d42e36815-true-d4088538e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43'] | self::hl7:id | self::hl7:code[(@code = '68518-0' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d4088245e12-true-d4088878e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088245e12-true-d4088878e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43'] | hl7:id | hl7:code[(@code = '68518-0' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d4088245e12-true-d4088878e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d4088245e45-true-d4088920e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088245e45-true-d4088920e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d4088245e45-true-d4088920e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4088245e88-true-d4089062e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088245e88-true-d4089062e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4088245e88-true-d4089062e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4088940e43-true-d4089170e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088940e43-true-d4089170e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4088940e43-true-d4089170e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4088940e62-true-d4089229e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088940e62-true-d4089229e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4088940e62-true-d4089229e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4088940e105-true-d4089286e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088940e105-true-d4089286e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4088940e105-true-d4089286e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4089290e91-true-d4089320e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4089290e91-true-d4089320e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4089290e91-true-d4089320e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4088940e128-true-d4089368e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088940e128-true-d4089368e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4088940e128-true-d4089368e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4088940e144-true-d4089413e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088940e144-true-d4089413e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4088940e144-true-d4089413e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4089383e66-true-d4089476e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089383e66-true-d4089476e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4089383e66-true-d4089476e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4088245e95-true-d4089644e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088245e95-true-d4089644e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4088245e95-true-d4089644e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4089521e15-true-d4089776e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089521e15-true-d4089776e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4089521e15-true-d4089776e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4089649e50-true-d4089842e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089649e50-true-d4089842e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4089649e50-true-d4089842e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4089649e120-true-d4089904e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089649e120-true-d4089904e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4089649e120-true-d4089904e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4089649e132-true-d4089926e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089649e132-true-d4089926e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4089649e132-true-d4089926e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4089914e12-true-d4089955e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089914e12-true-d4089955e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4089914e12-true-d4089955e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4089649e143-true-d4090006e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089649e143-true-d4090006e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4089649e143-true-d4090006e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4089980e58-true-d4090067e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089980e58-true-d4090067e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4089980e58-true-d4090067e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4089521e17-true-d4090144e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089521e17-true-d4090144e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4089521e17-true-d4090144e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4089521e26-true-d4090197e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089521e26-true-d4090197e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4089521e26-true-d4090197e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4089521e30-true-d4090252e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4089521e30-true-d4090252e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4089521e30-true-d4090252e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4090245e11-true-d4090279e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090245e11-true-d4090279e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4090245e11-true-d4090279e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d4088245e107-true-d4090333e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4088245e107-true-d4090333e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d4088245e107-true-d4090333e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d4090310e6-true-d4090366e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090310e6-true-d4090366e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d4090310e6-true-d4090366e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d4090386e54-true-d4090398e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d4090386e54-true-d4090398e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d4090386e54-true-d4090398e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']])]"
         id="d42e36830-true-d4090707e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36830-true-d4090707e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']] (rule-reference: d42e36830-true-d4090707e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42'] | self::hl7:id | self::hl7:code[(@code = '443315005' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed' or @nullFlavor] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d4090414e14-true-d4091047e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090414e14-true-d4091047e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42'] | hl7:id | hl7:code[(@code = '443315005' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed' or @nullFlavor] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d4090414e14-true-d4091047e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d4090414e47-true-d4091089e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090414e47-true-d4091089e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d4090414e47-true-d4091089e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4090414e73-true-d4091231e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090414e73-true-d4091231e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4090414e73-true-d4091231e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4091109e43-true-d4091339e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091109e43-true-d4091339e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4091109e43-true-d4091339e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4091109e62-true-d4091398e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091109e62-true-d4091398e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4091109e62-true-d4091398e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4091109e105-true-d4091455e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091109e105-true-d4091455e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4091109e105-true-d4091455e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4091459e91-true-d4091489e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4091459e91-true-d4091489e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4091459e91-true-d4091489e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4091109e128-true-d4091537e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091109e128-true-d4091537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4091109e128-true-d4091537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4091109e144-true-d4091582e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091109e144-true-d4091582e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4091109e144-true-d4091582e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4091552e66-true-d4091645e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091552e66-true-d4091645e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4091552e66-true-d4091645e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4090414e80-true-d4091813e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090414e80-true-d4091813e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4090414e80-true-d4091813e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4091690e15-true-d4091945e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091690e15-true-d4091945e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4091690e15-true-d4091945e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4091818e50-true-d4092011e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091818e50-true-d4092011e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4091818e50-true-d4092011e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4091818e120-true-d4092073e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091818e120-true-d4092073e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4091818e120-true-d4092073e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4091818e132-true-d4092095e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091818e132-true-d4092095e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4091818e132-true-d4092095e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4092083e12-true-d4092124e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092083e12-true-d4092124e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4092083e12-true-d4092124e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4091818e143-true-d4092175e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091818e143-true-d4092175e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4091818e143-true-d4092175e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4092149e58-true-d4092236e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092149e58-true-d4092236e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4092149e58-true-d4092236e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4091690e17-true-d4092313e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091690e17-true-d4092313e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4091690e17-true-d4092313e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4091690e26-true-d4092366e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091690e26-true-d4092366e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4091690e26-true-d4092366e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4091690e30-true-d4092421e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4091690e30-true-d4092421e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4091690e30-true-d4092421e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4092414e11-true-d4092448e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092414e11-true-d4092448e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4092414e11-true-d4092448e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d4090414e92-true-d4092502e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4090414e92-true-d4092502e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d4090414e92-true-d4092502e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d4092479e6-true-d4092535e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092479e6-true-d4092535e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d4092479e6-true-d4092535e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d4092555e54-true-d4092567e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d4092555e54-true-d4092567e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d4092555e54-true-d4092567e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e36847-true-d4092862e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e36847-true-d4092862e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e36847-true-d4092862e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d4092583e8-true-d4093175e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092583e8-true-d4093175e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d4092583e8-true-d4093175e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d4092583e57-true-d4093329e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092583e57-true-d4093329e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d4092583e57-true-d4093329e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d4093207e42-true-d4093437e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093207e42-true-d4093437e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d4093207e42-true-d4093437e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4093207e61-true-d4093496e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093207e61-true-d4093496e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4093207e61-true-d4093496e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4093207e104-true-d4093553e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093207e104-true-d4093553e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4093207e104-true-d4093553e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4093557e91-true-d4093587e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d4093557e91-true-d4093587e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4093557e91-true-d4093587e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d4093207e127-true-d4093635e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093207e127-true-d4093635e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d4093207e127-true-d4093635e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4093207e143-true-d4093680e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093207e143-true-d4093680e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4093207e143-true-d4093680e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d4093650e66-true-d4093743e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093650e66-true-d4093743e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d4093650e66-true-d4093743e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d4092583e63-true-d4093911e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4092583e63-true-d4093911e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d4092583e63-true-d4093911e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d4093788e5-true-d4094043e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093788e5-true-d4094043e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d4093788e5-true-d4094043e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4093916e50-true-d4094109e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093916e50-true-d4094109e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4093916e50-true-d4094109e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4093916e120-true-d4094171e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093916e120-true-d4094171e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4093916e120-true-d4094171e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d4093916e132-true-d4094193e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093916e132-true-d4094193e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d4093916e132-true-d4094193e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4094181e12-true-d4094222e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4094181e12-true-d4094222e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4094181e12-true-d4094222e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d4093916e143-true-d4094273e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093916e143-true-d4094273e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d4093916e143-true-d4094273e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4094247e58-true-d4094334e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4094247e58-true-d4094334e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4094247e58-true-d4094334e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d4093788e7-true-d4094411e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093788e7-true-d4094411e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d4093788e7-true-d4094411e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d4093788e16-true-d4094464e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093788e16-true-d4094464e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d4093788e16-true-d4094464e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d4093788e20-true-d4094519e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4093788e20-true-d4094519e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d4093788e20-true-d4094519e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d4094512e11-true-d4094546e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d4094512e11-true-d4094546e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d4094512e11-true-d4094546e0)</assert>
   </rule>
</pattern>
