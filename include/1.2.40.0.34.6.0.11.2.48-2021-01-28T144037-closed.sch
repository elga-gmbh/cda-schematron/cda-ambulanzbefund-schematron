<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.48
Name: Schwangerschaften - kodiert
Description: Die Sektion "Schwangerschaften" enthält Informationen über vergangene Schwangerschaften, Geburten und Abortus sowie aktuelle Schwangerschaft und erwarteten Geburtstermin.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037-closed">
   <title>Schwangerschaften - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']])]"
         id="d42e34446-true-d3952459e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34446-true-d3952459e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']] (rule-reference: d42e34446-true-d3952459e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '10162-6' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e34524-true-d3953377e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34524-true-d3953377e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '10162-6' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e34524-true-d3953377e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e34571-true-d3953535e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34571-true-d3953535e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e34571-true-d3953535e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3953413e43-true-d3953643e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953413e43-true-d3953643e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3953413e43-true-d3953643e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3953413e62-true-d3953702e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953413e62-true-d3953702e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3953413e62-true-d3953702e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3953413e105-true-d3953759e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953413e105-true-d3953759e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3953413e105-true-d3953759e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3953763e91-true-d3953793e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3953763e91-true-d3953793e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3953763e91-true-d3953793e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3953413e128-true-d3953841e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953413e128-true-d3953841e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3953413e128-true-d3953841e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3953413e144-true-d3953886e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953413e144-true-d3953886e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3953413e144-true-d3953886e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3953856e66-true-d3953949e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953856e66-true-d3953949e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3953856e66-true-d3953949e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e34578-true-d3954117e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34578-true-d3954117e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e34578-true-d3954117e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3953994e16-true-d3954249e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953994e16-true-d3954249e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3953994e16-true-d3954249e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3954122e50-true-d3954315e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954122e50-true-d3954315e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3954122e50-true-d3954315e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3954122e120-true-d3954377e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954122e120-true-d3954377e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3954122e120-true-d3954377e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3954122e132-true-d3954399e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954122e132-true-d3954399e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3954122e132-true-d3954399e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3954387e12-true-d3954428e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954387e12-true-d3954428e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3954387e12-true-d3954428e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3954122e143-true-d3954479e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954122e143-true-d3954479e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3954122e143-true-d3954479e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3954453e58-true-d3954540e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954453e58-true-d3954540e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3954453e58-true-d3954540e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3953994e18-true-d3954617e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953994e18-true-d3954617e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3953994e18-true-d3954617e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3953994e27-true-d3954670e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953994e27-true-d3954670e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3953994e27-true-d3954670e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3953994e31-true-d3954725e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3953994e31-true-d3954725e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3953994e31-true-d3954725e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3954718e11-true-d3954752e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954718e11-true-d3954752e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3954718e11-true-d3954752e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']])]"
         id="d42e34591-true-d3955121e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34591-true-d3955121e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']] (rule-reference: d42e34591-true-d3955121e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27'] | self::hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d3954783e14-true-d3955523e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e14-true-d3955523e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27'] | hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d3954783e14-true-d3955523e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation | self::ips:designation)]"
         id="d3954783e33-true-d3955562e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e33-true-d3955562e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation | ips:designation (rule-reference: d3954783e33-true-d3955562e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:code[(@code = '82810-3' and @codeSystem = '2.16.840.1.113883.6.1')]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3955566e41-true-d3955578e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d3955566e41-true-d3955578e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3955566e41-true-d3955578e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3955597e54-true-d3955609e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d3955597e54-true-d3955609e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3955597e54-true-d3955609e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[not(@nullFlavor)] | self::hl7:high)]"
         id="d3954783e77-true-d3955639e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e77-true-d3955639e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[not(@nullFlavor)] | hl7:high (rule-reference: d3954783e77-true-d3955639e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.52-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d3954783e91-true-d3955671e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e91-true-d3955671e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d3954783e91-true-d3955671e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d3954783e136-true-d3955808e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e136-true-d3955808e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d3954783e136-true-d3955808e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3955686e43-true-d3955916e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3955686e43-true-d3955916e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3955686e43-true-d3955916e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3955686e62-true-d3955975e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3955686e62-true-d3955975e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3955686e62-true-d3955975e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3955686e105-true-d3956032e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3955686e105-true-d3956032e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3955686e105-true-d3956032e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3956036e91-true-d3956066e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3956036e91-true-d3956066e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3956036e91-true-d3956066e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3955686e128-true-d3956114e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3955686e128-true-d3956114e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3955686e128-true-d3956114e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3955686e144-true-d3956159e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3955686e144-true-d3956159e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3955686e144-true-d3956159e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3956129e66-true-d3956222e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956129e66-true-d3956222e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3956129e66-true-d3956222e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d3954783e143-true-d3956390e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e143-true-d3956390e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d3954783e143-true-d3956390e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3956267e12-true-d3956522e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956267e12-true-d3956522e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3956267e12-true-d3956522e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3956395e50-true-d3956588e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956395e50-true-d3956588e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3956395e50-true-d3956588e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3956395e120-true-d3956650e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956395e120-true-d3956650e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3956395e120-true-d3956650e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3956395e132-true-d3956672e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956395e132-true-d3956672e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3956395e132-true-d3956672e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3956660e12-true-d3956701e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956660e12-true-d3956701e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3956660e12-true-d3956701e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3956395e143-true-d3956752e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956395e143-true-d3956752e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3956395e143-true-d3956752e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3956726e58-true-d3956813e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956726e58-true-d3956813e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3956726e58-true-d3956813e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3956267e14-true-d3956890e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956267e14-true-d3956890e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3956267e14-true-d3956890e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3956267e23-true-d3956943e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956267e23-true-d3956943e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3956267e23-true-d3956943e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3956267e27-true-d3956998e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956267e27-true-d3956998e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3956267e27-true-d3956998e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3956991e11-true-d3957025e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3956991e11-true-d3957025e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3956991e11-true-d3957025e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']])]"
         id="d3954783e153-true-d3957100e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e153-true-d3957100e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']] (rule-reference: d3954783e153-true-d3957100e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text | self::hl7:statusCode[@code = 'completed' or @nullFlavor] | self::hl7:effectiveTime | self::hl7:value | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d3957056e6-true-d3957187e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957056e6-true-d3957187e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text | hl7:statusCode[@code = 'completed' or @nullFlavor] | hl7:effectiveTime | hl7:value | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d3957056e6-true-d3957187e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation | self::ips:designation)]"
         id="d3957056e26-true-d3957228e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957056e26-true-d3957228e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation | ips:designation (rule-reference: d3957056e26-true-d3957228e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.202-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3957232e41-true-d3957244e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d3957232e41-true-d3957244e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3957232e41-true-d3957244e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3957263e54-true-d3957275e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d3957263e54-true-d3957275e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3957263e54-true-d3957275e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d3957056e96-true-d3957324e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957056e96-true-d3957324e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d3957056e96-true-d3957324e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d3957301e6-true-d3957357e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957301e6-true-d3957357e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d3957301e6-true-d3957357e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.45'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.29']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3957377e54-true-d3957389e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d3957377e54-true-d3957389e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3957377e54-true-d3957389e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d3954783e161-true-d3957428e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3954783e161-true-d3957428e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d3954783e161-true-d3957428e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d3957405e6-true-d3957461e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957405e6-true-d3957461e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d3957405e6-true-d3957461e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.44'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.27']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3957481e54-true-d3957493e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d3957481e54-true-d3957493e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3957481e54-true-d3957493e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']])]"
         id="d42e34609-true-d3957817e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34609-true-d3957817e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']] (rule-reference: d42e34609-true-d3957817e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d3957509e12-true-d3958173e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957509e12-true-d3958173e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d3957509e12-true-d3958173e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation | self::ips:designation)]"
         id="d3957509e37-true-d3958214e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957509e37-true-d3958214e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation | ips:designation (rule-reference: d3957509e37-true-d3958214e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3958218e41-true-d3958230e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d3958218e41-true-d3958230e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3958218e41-true-d3958230e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3958249e54-true-d3958261e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d3958249e54-true-d3958261e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3958249e54-true-d3958261e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d3957509e97-true-d3958404e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957509e97-true-d3958404e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d3957509e97-true-d3958404e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3958282e43-true-d3958512e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958282e43-true-d3958512e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3958282e43-true-d3958512e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3958282e62-true-d3958571e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958282e62-true-d3958571e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3958282e62-true-d3958571e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3958282e105-true-d3958628e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958282e105-true-d3958628e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3958282e105-true-d3958628e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3958632e91-true-d3958662e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3958632e91-true-d3958662e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3958632e91-true-d3958662e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3958282e128-true-d3958710e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958282e128-true-d3958710e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3958282e128-true-d3958710e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3958282e144-true-d3958755e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958282e144-true-d3958755e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3958282e144-true-d3958755e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3958725e66-true-d3958818e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958725e66-true-d3958818e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3958725e66-true-d3958818e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d3957509e104-true-d3958986e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957509e104-true-d3958986e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d3957509e104-true-d3958986e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3958863e15-true-d3959118e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958863e15-true-d3959118e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3958863e15-true-d3959118e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3958991e50-true-d3959184e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958991e50-true-d3959184e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3958991e50-true-d3959184e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3958991e120-true-d3959246e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958991e120-true-d3959246e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3958991e120-true-d3959246e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3958991e132-true-d3959268e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958991e132-true-d3959268e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3958991e132-true-d3959268e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3959256e12-true-d3959297e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959256e12-true-d3959297e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3959256e12-true-d3959297e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3958991e143-true-d3959348e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958991e143-true-d3959348e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3958991e143-true-d3959348e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3959322e58-true-d3959409e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959322e58-true-d3959409e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3959322e58-true-d3959409e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3958863e17-true-d3959486e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958863e17-true-d3959486e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3958863e17-true-d3959486e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3958863e26-true-d3959539e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958863e26-true-d3959539e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3958863e26-true-d3959539e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3958863e30-true-d3959594e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3958863e30-true-d3959594e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3958863e30-true-d3959594e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3959587e11-true-d3959621e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959587e11-true-d3959621e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3959587e11-true-d3959621e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d3957509e116-true-d3959675e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3957509e116-true-d3959675e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d3957509e116-true-d3959675e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d3959652e6-true-d3959708e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959652e6-true-d3959708e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d3959652e6-true-d3959708e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d3959728e54-true-d3959740e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d3959728e54-true-d3959740e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d3959728e54-true-d3959740e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e34624-true-d3960035e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d42e34624-true-d3960035e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e34624-true-d3960035e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d3959756e8-true-d3960348e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959756e8-true-d3960348e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d3959756e8-true-d3960348e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d3959756e57-true-d3960502e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959756e57-true-d3960502e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d3959756e57-true-d3960502e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3960380e42-true-d3960610e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960380e42-true-d3960610e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3960380e42-true-d3960610e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3960380e61-true-d3960669e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960380e61-true-d3960669e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3960380e61-true-d3960669e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3960380e104-true-d3960726e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960380e104-true-d3960726e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3960380e104-true-d3960726e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3960730e91-true-d3960760e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3960730e91-true-d3960760e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3960730e91-true-d3960760e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3960380e127-true-d3960808e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960380e127-true-d3960808e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3960380e127-true-d3960808e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3960380e143-true-d3960853e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960380e143-true-d3960853e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3960380e143-true-d3960853e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3960823e66-true-d3960916e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960823e66-true-d3960916e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3960823e66-true-d3960916e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d3959756e63-true-d3961084e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3959756e63-true-d3961084e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d3959756e63-true-d3961084e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3960961e5-true-d3961216e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960961e5-true-d3961216e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3960961e5-true-d3961216e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3961089e50-true-d3961282e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961089e50-true-d3961282e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3961089e50-true-d3961282e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3961089e120-true-d3961344e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961089e120-true-d3961344e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3961089e120-true-d3961344e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3961089e132-true-d3961366e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961089e132-true-d3961366e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3961089e132-true-d3961366e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3961354e12-true-d3961395e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961354e12-true-d3961395e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3961354e12-true-d3961395e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3961089e143-true-d3961446e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961089e143-true-d3961446e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3961089e143-true-d3961446e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3961420e58-true-d3961507e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961420e58-true-d3961507e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3961420e58-true-d3961507e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3960961e7-true-d3961584e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960961e7-true-d3961584e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3960961e7-true-d3961584e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3960961e16-true-d3961637e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960961e16-true-d3961637e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3960961e16-true-d3961637e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3960961e20-true-d3961692e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3960961e20-true-d3961692e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3960961e20-true-d3961692e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.48'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.11']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3961685e11-true-d3961719e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.html"
              test="not(.)">(atcdabrr_section_SchwangerschaftenKodiert)/d3961685e11-true-d3961719e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3961685e11-true-d3961719e0)</assert>
   </rule>
</pattern>
