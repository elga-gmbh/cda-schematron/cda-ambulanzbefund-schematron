<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.44
Name: Konservative Therapie
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018-closed">
   <title>Konservative Therapie</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']])]"
         id="d42e32864-true-d3795786e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d42e32864-true-d3795786e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']] (rule-reference: d42e32864-true-d3795786e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '281131004' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e32897-true-d3796270e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d42e32897-true-d3796270e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '281131004' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e32897-true-d3796270e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e32928-true-d3796422e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d42e32928-true-d3796422e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e32928-true-d3796422e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3796300e42-true-d3796530e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796300e42-true-d3796530e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3796300e42-true-d3796530e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3796300e61-true-d3796589e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796300e61-true-d3796589e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3796300e61-true-d3796589e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3796300e104-true-d3796646e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796300e104-true-d3796646e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3796300e104-true-d3796646e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3796650e91-true-d3796680e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3796650e91-true-d3796680e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3796650e91-true-d3796680e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3796300e127-true-d3796728e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796300e127-true-d3796728e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3796300e127-true-d3796728e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3796300e143-true-d3796773e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796300e143-true-d3796773e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3796300e143-true-d3796773e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3796743e66-true-d3796836e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796743e66-true-d3796836e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3796743e66-true-d3796836e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e32934-true-d3797004e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d42e32934-true-d3797004e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e32934-true-d3797004e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3796881e15-true-d3797136e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796881e15-true-d3797136e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3796881e15-true-d3797136e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3797009e50-true-d3797202e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797009e50-true-d3797202e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3797009e50-true-d3797202e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3797009e120-true-d3797264e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797009e120-true-d3797264e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3797009e120-true-d3797264e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3797009e132-true-d3797286e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797009e132-true-d3797286e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3797009e132-true-d3797286e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3797274e12-true-d3797315e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797274e12-true-d3797315e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3797274e12-true-d3797315e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3797009e143-true-d3797366e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797009e143-true-d3797366e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3797009e143-true-d3797366e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3797340e58-true-d3797427e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797340e58-true-d3797427e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3797340e58-true-d3797427e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3796881e17-true-d3797504e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796881e17-true-d3797504e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3796881e17-true-d3797504e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3796881e26-true-d3797557e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796881e26-true-d3797557e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3796881e26-true-d3797557e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3796881e30-true-d3797612e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3796881e30-true-d3797612e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3796881e30-true-d3797612e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3797605e11-true-d3797639e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797605e11-true-d3797639e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3797605e11-true-d3797639e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e32946-true-d3797949e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d42e32946-true-d3797949e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e32946-true-d3797949e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d3797670e8-true-d3798262e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797670e8-true-d3798262e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d3797670e8-true-d3798262e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d3797670e57-true-d3798416e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797670e57-true-d3798416e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d3797670e57-true-d3798416e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d3798294e42-true-d3798524e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798294e42-true-d3798524e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d3798294e42-true-d3798524e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3798294e61-true-d3798583e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798294e61-true-d3798583e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3798294e61-true-d3798583e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3798294e104-true-d3798640e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798294e104-true-d3798640e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3798294e104-true-d3798640e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3798644e91-true-d3798674e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d3798644e91-true-d3798674e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3798644e91-true-d3798674e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d3798294e127-true-d3798722e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798294e127-true-d3798722e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d3798294e127-true-d3798722e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3798294e143-true-d3798767e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798294e143-true-d3798767e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3798294e143-true-d3798767e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d3798737e66-true-d3798830e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798737e66-true-d3798830e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d3798737e66-true-d3798830e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d3797670e63-true-d3798998e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3797670e63-true-d3798998e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d3797670e63-true-d3798998e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d3798875e5-true-d3799130e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798875e5-true-d3799130e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d3798875e5-true-d3799130e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3799003e50-true-d3799196e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799003e50-true-d3799196e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3799003e50-true-d3799196e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3799003e120-true-d3799258e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799003e120-true-d3799258e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3799003e120-true-d3799258e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d3799003e132-true-d3799280e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799003e132-true-d3799280e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d3799003e132-true-d3799280e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3799268e12-true-d3799309e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799268e12-true-d3799309e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3799268e12-true-d3799309e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d3799003e143-true-d3799360e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799003e143-true-d3799360e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d3799003e143-true-d3799360e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3799334e58-true-d3799421e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799334e58-true-d3799421e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3799334e58-true-d3799421e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d3798875e7-true-d3799498e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798875e7-true-d3799498e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d3798875e7-true-d3799498e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d3798875e16-true-d3799551e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798875e16-true-d3799551e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d3798875e16-true-d3799551e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d3798875e20-true-d3799606e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3798875e20-true-d3799606e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d3798875e20-true-d3799606e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.44']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d3799599e11-true-d3799633e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.html"
              test="not(.)">(elgagab_section_konservativeTherapie)/d3799599e11-true-d3799633e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d3799599e11-true-d3799633e0)</assert>
   </rule>
</pattern>
