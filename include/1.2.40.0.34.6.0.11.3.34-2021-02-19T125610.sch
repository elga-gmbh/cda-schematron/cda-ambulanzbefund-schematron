<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.34
Name: Reaction Manifestation
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610">
   <title>Reaction Manifestation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]"
         id="d42e64800-false-d5429367e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="@negationInd">(atcdabbr_entry_ReactionManifestation): Attribut @negationInd MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="not(@negationInd) or (string-length(@negationInd) &gt; 0 and not(matches(@negationInd,'\s')))">(atcdabbr_entry_ReactionManifestation): Attribute @negationInd MUSS vom Datentyp 'cs' sein  - '<value-of select="@negationInd"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_ReactionManifestation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@moodCode) = ('EVN')">(atcdabbr_entry_ReactionManifestation): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34']) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34']) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:effectiveTime[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:effectiveTime[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:value[not(@nullFlavor)] | hl7:value[@nullFlavor='NI'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="$elmcount &gt;= 1">(atcdabbr_entry_ReactionManifestation): Auswahl (hl7:value[not(@nullFlavor)]  oder  hl7:value[@nullFlavor='NI']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="$elmcount &lt;= 1">(atcdabbr_entry_ReactionManifestation): Auswahl (hl7:value[not(@nullFlavor)]  oder  hl7:value[@nullFlavor='NI']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:value[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:value[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:value[@nullFlavor='NI']) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:value[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34']
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34']"
         id="d42e64814-false-d5429486e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.34')">(atcdabbr_entry_ReactionManifestation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.34' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']"
         id="d42e64822-false-d5429501e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@root) = ('2.16.840.1.113883.10.22.4.6')">(atcdabbr_entry_ReactionManifestation): Der Wert von root MUSS '2.16.840.1.113883.10.22.4.6' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:id[not(@nullFlavor)]"
         id="d42e64831-false-d5429515e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e64837-false-d5429528e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atcdabbr_entry_ReactionManifestation): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.35 atcdabbr_Problemarten_VS (DYNAMIC)' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:text
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:text"
         id="d5429529e69-false-d5429549e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d5429529e71-false-d5429568e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="@value">(atcdabrr_other_NarrativeTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="starts-with(@value,'#') or starts-with(@value,'http')">(atcdabrr_other_NarrativeTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element, or begin with the 'http' or 'https' url-scheme.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:statusCode[@code = 'completed']
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:statusCode[@code = 'completed']"
         id="d42e64851-false-d5429583e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="@nullFlavor or (@code='completed')">(atcdabbr_entry_ReactionManifestation): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:effectiveTime[not(@nullFlavor)]
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:effectiveTime[not(@nullFlavor)]"
         id="d42e64856-false-d5429599e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:low) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:low ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:low) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:low kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:high) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:high kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low"
         id="d42e64861-false-d5429624e0">
      <extends rule="TS.DATE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="not(*)">(atcdabbr_entry_ReactionManifestation): <value-of select="local-name()"/> with datatype TS.DATE, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high"
         id="d42e64867-false-d5429637e0">
      <extends rule="TS.DATE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="not(*)">(atcdabbr_entry_ReactionManifestation): <value-of select="local-name()"/> with datatype TS.DATE, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]"
         id="d42e64882-false-d5429648e0">
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="@code">(atcdabbr_entry_ReactionManifestation): Attribut @code MUSS vorkommen.</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@code),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="not(@code) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.10.181-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_entry_ReactionManifestation): Der Wert von code MUSS gewählt werden aus Value Set '1.2.40.0.34.10.181' atcdabbr_AllergyReaction_VS (DYNAMIC).</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:originalText[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:originalText[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="count(hl7:originalText[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_ReactionManifestation): Element hl7:originalText[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]/hl7:translation
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]/hl7:translation"
         id="d42e64891-false-d5429694e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.2
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]/hl7:originalText
Item: (atcdabbr_other_OriginalTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]/hl7:originalText"
         id="d5429695e52-false-d5429705e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabbr_other_OriginalTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OriginalTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OriginalTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.2
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]/hl7:originalText/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_other_OriginalTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[not(@nullFlavor)]/hl7:originalText/hl7:reference[not(@nullFlavor)]"
         id="d5429695e58-false-d5429724e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OriginalTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="@value">(atcdabbr_other_OriginalTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="starts-with(@value,'#')">(atcdabbr_other_OriginalTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[@nullFlavor='NI']
Item: (atcdabbr_entry_ReactionManifestation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:value[@nullFlavor='NI']"
         id="d42e64902-false-d5429736e0">
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_ReactionManifestation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@nullFlavor) = ('NI')">(atcdabbr_entry_ReactionManifestation): Der Wert von nullFlavor MUSS 'NI' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.34
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]]
Item: (atcdabbr_entry_ReactionManifestation)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.34'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.6']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.38'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.1']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_ReactionManifestation): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@typeCode) = ('SUBJ')">(atcdabbr_entry_ReactionManifestation): Der Wert von typeCode MUSS 'SUBJ' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.html"
              test="string(@inversionInd) = ('true')">(atcdabbr_entry_ReactionManifestation): Der Wert von inversionInd MUSS 'true' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
   </rule>
</pattern>
