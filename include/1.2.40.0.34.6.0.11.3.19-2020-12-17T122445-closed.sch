<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.19
Name: Eingebettetes Objekt Entry
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445-closed">
   <title>Eingebettetes Objekt Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']])]"
         id="d42e57964-true-d5215099e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e57964-true-d5215099e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']] (rule-reference: d42e57964-true-d5215099e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19'] | self::hl7:value[not(@nullFlavor)] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d42e58001-true-d5215587e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e58001-true-d5215587e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19'] | hl7:value[not(@nullFlavor)] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d42e58001-true-d5215587e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d42e58053-true-d5215742e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e58053-true-d5215742e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d42e58053-true-d5215742e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5215603e14-true-d5215848e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5215603e14-true-d5215848e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5215603e14-true-d5215848e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5215852e101-true-d5215921e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5215852e101-true-d5215921e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5215852e101-true-d5215921e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5215852e174-true-d5215997e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d5215852e174-true-d5215997e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5215852e174-true-d5215997e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5215852e186-true-d5216019e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d5215852e186-true-d5216019e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5215852e186-true-d5216019e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5216007e12-true-d5216048e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d5216007e12-true-d5216048e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5216007e12-true-d5216048e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5215852e198-true-d5216099e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5215852e198-true-d5216099e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5215852e198-true-d5216099e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5216073e58-true-d5216160e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216073e58-true-d5216160e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5216073e58-true-d5216160e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e58055-true-d5216327e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e58055-true-d5216327e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e58055-true-d5216327e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d5216205e38-true-d5216435e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216205e38-true-d5216435e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d5216205e38-true-d5216435e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5216205e57-true-d5216494e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216205e57-true-d5216494e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5216205e57-true-d5216494e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5216205e100-true-d5216551e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216205e100-true-d5216551e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5216205e100-true-d5216551e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5216555e91-true-d5216585e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d5216555e91-true-d5216585e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5216555e91-true-d5216585e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d5216205e123-true-d5216633e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216205e123-true-d5216633e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d5216205e123-true-d5216633e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5216205e139-true-d5216678e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216205e139-true-d5216678e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5216205e139-true-d5216678e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5216648e66-true-d5216741e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216648e66-true-d5216741e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5216648e66-true-d5216741e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e58057-true-d5216909e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e58057-true-d5216909e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e58057-true-d5216909e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5216786e5-true-d5217041e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216786e5-true-d5217041e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5216786e5-true-d5217041e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5216914e50-true-d5217107e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216914e50-true-d5217107e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5216914e50-true-d5217107e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5216914e120-true-d5217169e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216914e120-true-d5217169e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5216914e120-true-d5217169e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5216914e132-true-d5217191e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216914e132-true-d5217191e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5216914e132-true-d5217191e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5217179e12-true-d5217220e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217179e12-true-d5217220e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5217179e12-true-d5217220e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5216914e143-true-d5217271e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216914e143-true-d5217271e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5216914e143-true-d5217271e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5217245e58-true-d5217332e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217245e58-true-d5217332e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5217245e58-true-d5217332e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d5216786e7-true-d5217409e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216786e7-true-d5217409e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d5216786e7-true-d5217409e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5216786e16-true-d5217462e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216786e16-true-d5217462e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5216786e16-true-d5217462e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5216786e20-true-d5217517e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5216786e20-true-d5217517e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5216786e20-true-d5217517e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5217510e11-true-d5217544e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217510e11-true-d5217544e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5217510e11-true-d5217544e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d42e58059-true-d5217672e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e58059-true-d5217672e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d42e58059-true-d5217672e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d5217575e11-true-d5217780e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217575e11-true-d5217780e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d5217575e11-true-d5217780e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5217575e22-true-d5217842e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217575e22-true-d5217842e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5217575e22-true-d5217842e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d5217575e78-true-d5217920e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217575e78-true-d5217920e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d5217575e78-true-d5217920e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d5217575e80-true-d5217981e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217575e80-true-d5217981e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d5217575e80-true-d5217981e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:desc)]"
         id="d5217575e83-true-d5218039e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.19-2020-12-17T122445.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d5217575e83-true-d5218039e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:desc (rule-reference: d5217575e83-true-d5218039e0)</assert>
   </rule>
</pattern>
