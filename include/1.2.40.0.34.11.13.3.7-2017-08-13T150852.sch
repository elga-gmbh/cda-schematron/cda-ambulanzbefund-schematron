<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.13.3.7
Name: Problem Entry Gesundheitsproblem
Description: Mit dieser Observation wird ein bekanntes relevantes Gesundheitsproblem des Patienten codiert dargestellt. Die Zeitspanne, in der ein Gesundheitsproblem besteht oder bestanden hat, wird hier angegeben.  
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.13.3.7-2017-08-13T150852">
   <title>Problem Entry Gesundheitsproblem</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]"
         id="d42e1587-false-d7547e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]"
         id="d42e1653-false-d7584e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@classCode) = ('OBS')">(ProblemEntryGesundheitsproblem): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@moodCode) = ('EVN')">(ProblemEntryGesundheitsproblem): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.7'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.7'] kommt zu häufig vor [max 1x].</assert>
      <report fpi="CD-UNKN-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt; 1">(ProblemEntryGesundheitsproblem): Element hl7:code ist codiert mit Bindungsstärke 'extensible' und enthält ein Code außerhalb des angegebene Wertraums.</report>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:effectiveTime[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:effectiveTime[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:value[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] | hl7:value[@nullFlavor='NI'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="$elmcount &gt;= 1">(ProblemEntryGesundheitsproblem): Auswahl (hl7:value[not(@nullFlavor)]  oder  hl7:value[not(@nullFlavor)]  oder  hl7:value[@nullFlavor='NI']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="$elmcount &lt;= 1">(ProblemEntryGesundheitsproblem): Auswahl (hl7:value[not(@nullFlavor)]  oder  hl7:value[not(@nullFlavor)]  oder  hl7:value[@nullFlavor='NI']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:value[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:value[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:value[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:value[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:value[@nullFlavor='NI']) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:value[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']"
         id="d42e1659-false-d7698e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@root) = ('1.2.40.0.34.11.13.3.7')">(ProblemEntryGesundheitsproblem): Der Wert von root MUSS '1.2.40.0.34.11.13.3.7' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:id
Item: (ProblemEntryGesundheitsproblem)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e1670-false-d7723e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.35-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(ProblemEntryGesundheitsproblem): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.35 atcdabbr_Problemarten_VS (DYNAMIC)' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:statusCode[@code = 'completed']
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:statusCode[@code = 'completed']"
         id="d42e1686-false-d7744e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@nullFlavor or (@code='completed')">(ProblemEntryGesundheitsproblem): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]"
         id="d42e1694-false-d7760e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:low) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:low ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:low) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:low kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:high) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:high kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low"
         id="d42e1696-false-d7785e0">
      <extends rule="IVXB_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVXB_TS')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVXB_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high"
         id="d42e1702-false-d7795e0">
      <extends rule="IVXB_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVXB_TS')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVXB_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]"
         id="d42e1759-false-d7803e0">
      <extends rule="CD"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@code">(ProblemEntryGesundheitsproblem): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(ProblemEntryGesundheitsproblem): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@codeSystem">(ProblemEntryGesundheitsproblem): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(ProblemEntryGesundheitsproblem): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:originalText) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:originalText kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText"
         id="d42e1797-false-d7835e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText/hl7:reference[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText/hl7:reference[not(@nullFlavor)]"
         id="d42e1799-false-d7854e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:translation
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:translation"
         id="d42e1807-false-d7864e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]"
         id="d42e1820-false-d7872e0">
      <extends rule="CD"/>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.179-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(ProblemEntryGesundheitsproblem): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.179 ELGA_AbsentOrUnknownProblems (DYNAMIC)' sein.</assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@code">(ProblemEntryGesundheitsproblem): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(ProblemEntryGesundheitsproblem): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@codeSystem">(ProblemEntryGesundheitsproblem): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(ProblemEntryGesundheitsproblem): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:originalText) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:originalText kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText"
         id="d42e1834-false-d7913e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText/hl7:reference[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:originalText/hl7:reference[not(@nullFlavor)]"
         id="d42e1836-false-d7932e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:translation
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[not(@nullFlavor)]/hl7:translation"
         id="d42e1843-false-d7942e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']"
         id="d42e1856-false-d7950e0">
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:originalText[not(@nullFlavor)]) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:originalText[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:originalText[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:originalText[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']/hl7:originalText[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']/hl7:originalText[not(@nullFlavor)]"
         id="d42e1869-false-d7971e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(ProblemEntryGesundheitsproblem): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']/hl7:originalText[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]
Item: (ProblemEntryGesundheitsproblem)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:value[@nullFlavor='NI']/hl7:originalText[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]"
         id="d42e1871-false-d7990e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(ProblemEntryGesundheitsproblem): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]]
Item: (ProblemEntryGesundheitsproblem)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.21']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@typeCode) = ('SUBJ')">(ProblemEntryGesundheitsproblem): Der Wert von typeCode MUSS 'SUBJ' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@inversionInd) = ('true')">(ProblemEntryGesundheitsproblem): Der Wert von inversionInd MUSS 'true' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]
Item: (ProblemEntryGesundheitsproblem)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@typeCode) = ('SUBJ')">(ProblemEntryGesundheitsproblem): Der Wert von typeCode MUSS 'SUBJ' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@inversionInd) = ('true')">(ProblemEntryGesundheitsproblem): Der Wert von inversionInd MUSS 'true' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]
Item: (ProblemEntryGesundheitsproblem)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@typeCode) = ('SUBJ')">(ProblemEntryGesundheitsproblem): Der Wert von typeCode MUSS 'SUBJ' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@inversionInd) = ('true')">(ProblemEntryGesundheitsproblem): Der Wert von inversionInd MUSS 'true' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.7
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]]
Item: (ProblemEntryGesundheitsproblem)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.7']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.13']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@typeCode) = ('REFR')">(ProblemEntryGesundheitsproblem): Der Wert von typeCode MUSS 'REFR' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20231018T125019/tmp-1.2.40.0.34.11.13.3.7-2017-08-13T150852.html"
              test="string(@inversionInd) = ('false')">(ProblemEntryGesundheitsproblem): Der Wert von inversionInd MUSS 'false' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
   </rule>
</pattern>
