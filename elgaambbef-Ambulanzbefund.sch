<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>Schematron file for transaction Ambulanzbefund 2020 (1.2.40.0.34.777.8.4.2 2020-01-15T10:46:22)</title>
   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
   <ns uri="urn:hl7-org:v3" prefix="cda"/>
   <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <ns uri="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
   <!-- Add extra namespaces -->
   <ns uri="urn:hl7-at:v3" prefix="hl7at"/>
   <ns uri="urn:ihe:pharm:medication" prefix="pharm"/>
   <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <ns uri="urn:hl7-org:ips" prefix="ips"/>
   <ns uri="urn:hl7-org:sdtc" prefix="sdtc"/>
   <!-- Include realm specific schematron -->
   <!-- Include datatype abstract schematrons -->
   <pattern>
      <include href="include/DTr1_AD.sch"/>
      <include href="include/DTr1_AD.CA.sch"/>
      <include href="include/DTr1_AD.CA.BASIC.sch"/>
      <include href="include/DTr1_AD.DE.sch"/>
      <include href="include/DTr1_AD.EPSOS.sch"/>
      <include href="include/DTr1_AD.IPS.sch"/>
      <include href="include/DTr1_AD.NL.sch"/>
      <include href="include/DTr1_ADXP.sch"/>
      <include href="include/DTr1_ANY.sch"/>
      <include href="include/DTr1_BIN.sch"/>
      <include href="include/DTr1_BL.sch"/>
      <include href="include/DTr1_BN.sch"/>
      <include href="include/DTr1_BXIT_IVL_PQ.sch"/>
      <include href="include/DTr1_CD.sch"/>
      <include href="include/DTr1_CD.EPSOS.sch"/>
      <include href="include/DTr1_CD.IPS.sch"/>
      <include href="include/DTr1_CD.SDTC.sch"/>
      <include href="include/DTr1_CE.sch"/>
      <include href="include/DTr1_CE.EPSOS.sch"/>
      <include href="include/DTr1_CE.IPS.sch"/>
      <include href="include/DTr1_CO.sch"/>
      <include href="include/DTr1_CO.EPSOS.sch"/>
      <include href="include/DTr1_CR.sch"/>
      <include href="include/DTr1_CS.sch"/>
      <include href="include/DTr1_CS.LANG.sch"/>
      <include href="include/DTr1_CV.sch"/>
      <include href="include/DTr1_CV.EPSOS.sch"/>
      <include href="include/DTr1_CV.IPS.sch"/>
      <include href="include/DTr1_ED.sch"/>
      <include href="include/DTr1_EIVL.event.sch"/>
      <include href="include/DTr1_EIVL_TS.sch"/>
      <include href="include/DTr1_EN.sch"/>
      <include href="include/DTr1_ENXP.sch"/>
      <include href="include/DTr1_GLIST.sch"/>
      <include href="include/DTr1_GLIST_PQ.sch"/>
      <include href="include/DTr1_GLIST_TS.sch"/>
      <include href="include/DTr1_hl7nl-INT.sch"/>
      <include href="include/DTr1_hl7nl-IVL_QTY.sch"/>
      <include href="include/DTr1_hl7nl-IVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PIVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PQ.sch"/>
      <include href="include/DTr1_hl7nl-QSET_QTY.sch"/>
      <include href="include/DTr1_hl7nl-RTO.sch"/>
      <include href="include/DTr1_hl7nl-TS.sch"/>
      <include href="include/DTr1_II.sch"/>
      <include href="include/DTr1_II.AT.ATU.sch"/>
      <include href="include/DTr1_II.AT.BLZ.sch"/>
      <include href="include/DTr1_II.AT.DVR.sch"/>
      <include href="include/DTr1_II.AT.KTONR.sch"/>
      <include href="include/DTr1_II.EPSOS.sch"/>
      <include href="include/DTr1_II.NL.AGB.sch"/>
      <include href="include/DTr1_II.NL.BIG.sch"/>
      <include href="include/DTr1_II.NL.BSN.sch"/>
      <include href="include/DTr1_II.NL.URA.sch"/>
      <include href="include/DTr1_II.NL.UZI.sch"/>
      <include href="include/DTr1_INT.sch"/>
      <include href="include/DTr1_INT.NONNEG.sch"/>
      <include href="include/DTr1_INT.POS.sch"/>
      <include href="include/DTr1_IVL_INT.sch"/>
      <include href="include/DTr1_IVL_MO.sch"/>
      <include href="include/DTr1_IVL_PQ.sch"/>
      <include href="include/DTr1_IVL_REAL.sch"/>
      <include href="include/DTr1_IVL_TS.sch"/>
      <include href="include/DTr1_IVL_TS.CH.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_IVL_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_IVXB_INT.sch"/>
      <include href="include/DTr1_IVXB_MO.sch"/>
      <include href="include/DTr1_IVXB_PQ.sch"/>
      <include href="include/DTr1_IVXB_REAL.sch"/>
      <include href="include/DTr1_IVXB_TS.sch"/>
      <include href="include/DTr1_list_int.sch"/>
      <include href="include/DTr1_MO.sch"/>
      <include href="include/DTr1_ON.sch"/>
      <include href="include/DTr1_PIVL_TS.sch"/>
      <include href="include/DTr1_PN.sch"/>
      <include href="include/DTr1_PN.CA.sch"/>
      <include href="include/DTr1_PN.NL.sch"/>
      <include href="include/DTr1_PQ.sch"/>
      <include href="include/DTr1_PQR.sch"/>
      <include href="include/DTr1_QTY.sch"/>
      <include href="include/DTr1_REAL.sch"/>
      <include href="include/DTr1_REAL.NONNEG.sch"/>
      <include href="include/DTr1_REAL.POS.sch"/>
      <include href="include/DTr1_RTO.sch"/>
      <include href="include/DTr1_RTO_PQ_PQ.sch"/>
      <include href="include/DTr1_RTO_QTY_QTY.sch"/>
      <include href="include/DTr1_SC.sch"/>
      <include href="include/DTr1_SD.TEXT.sch"/>
      <include href="include/DTr1_SLIST.sch"/>
      <include href="include/DTr1_SLIST_PQ.sch"/>
      <include href="include/DTr1_SLIST_TS.sch"/>
      <include href="include/DTr1_ST.sch"/>
      <include href="include/DTr1_SXCM_INT.sch"/>
      <include href="include/DTr1_SXCM_MO.sch"/>
      <include href="include/DTr1_SXCM_PQ.sch"/>
      <include href="include/DTr1_SXCM_REAL.sch"/>
      <include href="include/DTr1_SXCM_TS.sch"/>
      <include href="include/DTr1_SXPR_TS.sch"/>
      <include href="include/DTr1_TEL.sch"/>
      <include href="include/DTr1_TEL.AT.sch"/>
      <include href="include/DTr1_TEL.CA.EMAIL.sch"/>
      <include href="include/DTr1_TEL.CA.PHONE.sch"/>
      <include href="include/DTr1_TEL.EPSOS.sch"/>
      <include href="include/DTr1_TEL.IPS.sch"/>
      <include href="include/DTr1_TEL.NL.EXTENDED.sch"/>
      <include href="include/DTr1_thumbnail.sch"/>
      <include href="include/DTr1_TN.sch"/>
      <include href="include/DTr1_TS.sch"/>
      <include href="include/DTr1_TS.AT.TZ.sch"/>
      <include href="include/DTr1_TS.AT.VAR.sch"/>
      <include href="include/DTr1_TS.CH.TZ.sch"/>
      <include href="include/DTr1_TS.DATE.sch"/>
      <include href="include/DTr1_TS.DATE.FULL.sch"/>
      <include href="include/DTr1_TS.DATE.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIME.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIMETZ.MIN.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_URL.sch"/>
      <include href="include/DTr1_URL.NL.EXTENDED.sch"/>
   </pattern>

   <!-- Include the project schematrons related to scenario Ambulanzbefund -->

   <!-- elgaambbef_document_ambulanzbefund -->
   <pattern>
      <title>elgaambbef_document_ambulanzbefund</title>
      <rule fpi="RUL-EXPEL" context="/">
         <assert role="warning"
                 test="descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.22.1'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.5']]"
                 see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.0.5-2022-06-07T105337.html">(elgaambbef_document_ambulanzbefund): Instance is expected to have the following element: descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.22.1'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.5']]</assert>
      </rule>
   </pattern>
   <include href="include/1.2.40.0.34.6.0.11.0.5-2022-06-07T105337.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.0.5-2022-06-07T105337-closed.sch"/>


   <!-- Create phases for more targeted validation on large instances -->
   <phase id="AllExceptClosed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.5-2022-06-07T105337"/>
      <active pattern="template-1.2.40.0.34.11.1.3.1-2017-05-05T000000"/>
      <active pattern="template-1.2.40.0.34.11.13.3.13-2017-07-13T210449"/>
      <active pattern="template-1.2.40.0.34.11.13.3.18-2017-08-10T195216"/>
      <active pattern="template-1.2.40.0.34.11.13.3.19-2017-08-10T202554"/>
      <active pattern="template-1.2.40.0.34.11.13.3.21-2017-08-20T120835"/>
      <active pattern="template-1.2.40.0.34.11.13.3.6-2017-01-26T142954"/>
      <active pattern="template-1.2.40.0.34.11.13.3.7-2017-08-13T150852"/>
      <active pattern="template-1.2.40.0.34.11.8.1.3.1-2014-09-01T000000"/>
      <active pattern="template-1.2.40.0.34.11.8.2.3.1-2014-09-10T000000"/>
      <active pattern="template-1.2.40.0.34.11.8.4.3.1-2014-03-04T000000"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.10-2021-02-19T084636"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.11-2021-02-19T104146"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.12-2021-02-19T104156"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.13-2021-02-19T090307"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.14-2021-02-19T103551"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.16-2021-02-19T090653"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.17-2021-02-19T092953"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.19-2021-02-19T090603"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.20-2021-02-19T103545"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.21-2021-02-19T090540"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.22-2021-02-19T090241"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.23-2021-02-19T104339"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.25-2021-02-19T104512"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.26-2021-02-19T104355"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.41-2021-02-19T084624"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.42-2021-02-19T104151"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.45-2021-02-19T090340"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.46-2021-02-19T120303"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.47-2021-02-19T093000"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.49-2021-02-19T103539"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.50-2021-02-19T103530"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.51-2021-02-19T090357"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.52-2021-02-19T090348"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.53-2021-02-19T093025"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.54-2021-02-19T092815"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.56-2021-02-19T090548"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.57-2021-02-19T104138"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.58-2021-02-19T104329"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.59-2021-02-19T114024"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.61-2021-02-19T120311"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.62-2021-02-19T115806"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.63-2021-02-19T115029"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.64-2021-02-19T104202"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.68-2021-02-19T104313"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2021-06-28T112240"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.72-2021-02-19T114054"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.73-2021-02-19T114423"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.74-2021-02-19T090717"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2021-06-28T112805"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.83-2021-02-23T062526"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.88-2021-02-19T104523"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.89-2021-02-19T104533"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.9-2021-02-19T084614"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.90-2021-02-19T104512"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.94-2021-02-19T114416"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2021-02-23T062231"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.97-2021-02-19T090321"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.98-2021-02-19T090557"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2022-01-25T142109"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2021-08-04T144727"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145841"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.104-2021-02-19T125919"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2021-02-19T124340"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2021-05-19T150517"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2021-06-28T111327"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2021-10-28T134914"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.23-2021-02-19T130106"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.24-2021-02-19T130055"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2022-03-10T141321"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.30-2021-02-19T104636"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.31-2023-02-02T154005"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.32-2021-02-19T124110"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.33-2021-02-19T123940"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.37-2021-02-19T124116"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.40-2021-02-19T125146"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.41-2021-02-19T125138"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.42-2021-02-19T125132"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.43-2021-02-19T125125"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.44-2022-03-14T112152"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.45-2021-02-01T140251"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.47-2021-02-19T124346"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2021-02-19T124642"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.50-2021-02-19T125214"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.51-2023-02-03T103752"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.72-2021-02-19T125222"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2022-07-12T085429"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2021-08-04T122820"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2022-01-25T134202"/>
   </phase>
   <phase id="elgaambbef_document_ambulanzbefund">
      <active pattern="template-1.2.40.0.34.6.0.11.0.5-2022-06-07T105337"/>
   </phase>
   <phase id="elgaambbef_document_ambulanzbefund-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.5-2022-06-07T105337-closed"/>
   </phase>
   <phase id="EingebettetesObjektEntry-20170505T000000">
      <active pattern="template-1.2.40.0.34.11.1.3.1-2017-05-05T000000"/>
   </phase>
   <phase id="ProblemStatusObservationEntry-20170713T210449">
      <active pattern="template-1.2.40.0.34.11.13.3.13-2017-07-13T210449"/>
   </phase>
   <phase id="CriticalityObservation">
      <active pattern="template-1.2.40.0.34.11.13.3.18-2017-08-10T195216"/>
   </phase>
   <phase id="CertaintyObservation">
      <active pattern="template-1.2.40.0.34.11.13.3.19-2017-08-10T202554"/>
   </phase>
   <phase id="SeverityObservation">
      <active pattern="template-1.2.40.0.34.11.13.3.21-2017-08-20T120835"/>
   </phase>
   <phase id="GesundheitsproblemBedenkenEntry">
      <active pattern="template-1.2.40.0.34.11.13.3.6-2017-01-26T142954"/>
   </phase>
   <phase id="ProblemEntryGesundheitsproblem-20170813T150852">
      <active pattern="template-1.2.40.0.34.11.13.3.7-2017-08-13T150852"/>
   </phase>
   <phase id="MedikationVerordnungEntryemed-20140901T000000">
      <active pattern="template-1.2.40.0.34.11.8.1.3.1-2014-09-01T000000"/>
   </phase>
   <phase id="MedikationAbgabeEntryemed-20140910T000000">
      <active pattern="template-1.2.40.0.34.11.8.2.3.1-2014-09-10T000000"/>
   </phase>
   <phase id="MedikationPharmazeutischeEmpfehlungEntry-20140304T000000">
      <active pattern="template-1.2.40.0.34.11.8.4.3.1-2014-03-04T000000"/>
   </phase>
   <phase id="atcdabrr_section_ImpfungenKodiert-20210219T114643">
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643"/>
   </phase>
   <phase id="atcdabrr_section_ImpfungenKodiert-20210219T114643-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643-closed"/>
   </phase>
   <phase id="elgagab_section_anamnese-20210219T084636">
      <active pattern="template-1.2.40.0.34.6.0.11.2.10-2021-02-19T084636"/>
   </phase>
   <phase id="elgagab_section_anamnese-20210219T084636-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.10-2021-02-19T084636-closed"/>
   </phase>
   <phase id="elgagab_section_diagnostikUndBefundeUnkodiert-20210219T104146">
      <active pattern="template-1.2.40.0.34.6.0.11.2.11-2021-02-19T104146"/>
   </phase>
   <phase id="elgagab_section_diagnostikUndBefundeUnkodiert-20210219T104146-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.11-2021-02-19T104146-closed"/>
   </phase>
   <phase id="elgagab_section_Verlauf-20210219T104156">
      <active pattern="template-1.2.40.0.34.6.0.11.2.12-2021-02-19T104156"/>
   </phase>
   <phase id="elgagab_section_Verlauf-20210219T104156-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.12-2021-02-19T104156-closed"/>
   </phase>
   <phase id="elgagab_section_durchgefuehrteMassnahmenKodiert-20210219T090307">
      <active pattern="template-1.2.40.0.34.6.0.11.2.13-2021-02-19T090307"/>
   </phase>
   <phase id="elgagab_section_durchgefuehrteMassnahmenKodiert-20210219T090307-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.13-2021-02-19T090307-closed"/>
   </phase>
   <phase id="elgagab_section_pflegemassnahmen-20210219T103551">
      <active pattern="template-1.2.40.0.34.6.0.11.2.14-2021-02-19T103551"/>
   </phase>
   <phase id="elgagab_section_pflegemassnahmen-20210219T103551-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.14-2021-02-19T103551-closed"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenUnkodiert-20210219T090653">
      <active pattern="template-1.2.40.0.34.6.0.11.2.16-2021-02-19T090653"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenUnkodiert-20210219T090653-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.16-2021-02-19T090653-closed"/>
   </phase>
   <phase id="elgagab_section_KonsultUeberweisungsgrundUnkodiert-20210219T092953">
      <active pattern="template-1.2.40.0.34.6.0.11.2.17-2021-02-19T092953"/>
   </phase>
   <phase id="elgagab_section_KonsultUeberweisungsgrundUnkodiert-20210219T092953-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.17-2021-02-19T092953-closed"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeDiagnostik-20210219T090603">
      <active pattern="template-1.2.40.0.34.6.0.11.2.19-2021-02-19T090603"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeDiagnostik-20210219T090603-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.19-2021-02-19T090603-closed"/>
   </phase>
   <phase id="elgagab_section_Status-20210219T103545">
      <active pattern="template-1.2.40.0.34.6.0.11.2.20-2021-02-19T103545"/>
   </phase>
   <phase id="elgagab_section_Status-20210219T103545-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.20-2021-02-19T103545-closed"/>
   </phase>
   <phase id="elgagab_section_EmpfohleneMedikationKodiert-20210219T090540">
      <active pattern="template-1.2.40.0.34.6.0.11.2.21-2021-02-19T090540"/>
   </phase>
   <phase id="elgagab_section_EmpfohleneMedikationKodiert-20210219T090540-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.21-2021-02-19T090540-closed"/>
   </phase>
   <phase id="elgagab_section_durchgefuehrteMassnahmenUnkodiert-20210219T090241">
      <active pattern="template-1.2.40.0.34.6.0.11.2.22-2021-02-19T090241"/>
   </phase>
   <phase id="elgagab_section_durchgefuehrteMassnahmenUnkodiert-20210219T090241-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.22-2021-02-19T090241-closed"/>
   </phase>
   <phase id="elgagab_section_weitereEmpfohleneMassnahmenUnkodiert-20210219T104339">
      <active pattern="template-1.2.40.0.34.6.0.11.2.23-2021-02-19T104339"/>
   </phase>
   <phase id="elgagab_section_weitereEmpfohleneMassnahmenUnkodiert-20210219T104339-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.23-2021-02-19T104339-closed"/>
   </phase>
   <phase id="elgagab_section_ZusammenfassendeBeurteilung-20210219T104512">
      <active pattern="template-1.2.40.0.34.6.0.11.2.25-2021-02-19T104512"/>
   </phase>
   <phase id="elgagab_section_ZusammenfassendeBeurteilung-20210219T104512-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.25-2021-02-19T104512-closed"/>
   </phase>
   <phase id="elgagab_section_WeitereInformationen-20210219T104355">
      <active pattern="template-1.2.40.0.34.6.0.11.2.26-2021-02-19T104355"/>
   </phase>
   <phase id="elgagab_section_WeitereInformationen-20210219T104355-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.26-2021-02-19T104355-closed"/>
   </phase>
   <phase id="elgagab_section_allergienIntoleranzenUnkodiert-20210219T084624">
      <active pattern="template-1.2.40.0.34.6.0.11.2.41-2021-02-19T084624"/>
   </phase>
   <phase id="elgagab_section_allergienIntoleranzenUnkodiert-20210219T084624-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.41-2021-02-19T084624-closed"/>
   </phase>
   <phase id="elgagab_section_TermineKontrollenWiederbestellungen-20210219T104151">
      <active pattern="template-1.2.40.0.34.6.0.11.2.42-2021-02-19T104151"/>
   </phase>
   <phase id="elgagab_section_TermineKontrollenWiederbestellungen-20210219T104151-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.42-2021-02-19T104151-closed"/>
   </phase>
   <phase id="elgagab_section_geplanteUntersuchungen-20210219T090640">
      <active pattern="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640"/>
   </phase>
   <phase id="elgagab_section_geplanteUntersuchungen-20210219T090640-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640-closed"/>
   </phase>
   <phase id="elgagab_section_konservativeTherapie-20210219T093018">
      <active pattern="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018"/>
   </phase>
   <phase id="elgagab_section_konservativeTherapie-20210219T093018-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018-closed"/>
   </phase>
   <phase id="elgagab_section_chirurgischeTherapie-20210219T090340">
      <active pattern="template-1.2.40.0.34.6.0.11.2.45-2021-02-19T090340"/>
   </phase>
   <phase id="elgagab_section_chirurgischeTherapie-20210219T090340-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.45-2021-02-19T090340-closed"/>
   </phase>
   <phase id="atcdabrr_section_VitalparameterKodiert-20210219T120303">
      <active pattern="template-1.2.40.0.34.6.0.11.2.46-2021-02-19T120303"/>
   </phase>
   <phase id="atcdabrr_section_VitalparameterKodiert-20210219T120303-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.46-2021-02-19T120303-closed"/>
   </phase>
   <phase id="elgagab_section_KonsultUeberweisungsgrundKodiert-20210219T093000">
      <active pattern="template-1.2.40.0.34.6.0.11.2.47-2021-02-19T093000"/>
   </phase>
   <phase id="elgagab_section_KonsultUeberweisungsgrundKodiert-20210219T093000-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.47-2021-02-19T093000-closed"/>
   </phase>
   <phase id="atcdabrr_section_SchwangerschaftenKodiert-20210128T144037">
      <active pattern="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037"/>
   </phase>
   <phase id="atcdabrr_section_SchwangerschaftenKodiert-20210128T144037-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037-closed"/>
   </phase>
   <phase id="elgagab_section_SchwangerschaftenUnkodiert-20210219T103539">
      <active pattern="template-1.2.40.0.34.6.0.11.2.49-2021-02-19T103539"/>
   </phase>
   <phase id="elgagab_section_SchwangerschaftenUnkodiert-20210219T103539-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.49-2021-02-19T103539-closed"/>
   </phase>
   <phase id="elgagab_section_ImplantateUnkodiert-20210219T103530">
      <active pattern="template-1.2.40.0.34.6.0.11.2.50-2021-02-19T103530"/>
   </phase>
   <phase id="elgagab_section_ImplantateUnkodiert-20210219T103530-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.50-2021-02-19T103530-closed"/>
   </phase>
   <phase id="cdagab_section_BeeintraechtigungenKodiert-20210219T090357">
      <active pattern="template-1.2.40.0.34.6.0.11.2.51-2021-02-19T090357"/>
   </phase>
   <phase id="cdagab_section_BeeintraechtigungenKodiert-20210219T090357-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.51-2021-02-19T090357-closed"/>
   </phase>
   <phase id="elgagab_section_BeeintraechtigungenUnkodiert-20210219T090348">
      <active pattern="template-1.2.40.0.34.6.0.11.2.52-2021-02-19T090348"/>
   </phase>
   <phase id="elgagab_section_BeeintraechtigungenUnkodiert-20210219T090348-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.52-2021-02-19T090348-closed"/>
   </phase>
   <phase id="elgagab_section_ImpfungenUnkodiert-20210219T093025">
      <active pattern="template-1.2.40.0.34.6.0.11.2.53-2021-02-19T093025"/>
   </phase>
   <phase id="elgagab_section_ImpfungenUnkodiert-20210219T093025-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.53-2021-02-19T093025-closed"/>
   </phase>
   <phase id="elgagab_section_LebensstilUnkodiert-20210219T092815">
      <active pattern="template-1.2.40.0.34.6.0.11.2.54-2021-02-19T092815"/>
   </phase>
   <phase id="elgagab_section_LebensstilUnkodiert-20210219T092815-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.54-2021-02-19T092815-closed"/>
   </phase>
   <phase id="elgagab_section_LebensstilKodiert-20210219T092947">
      <active pattern="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947"/>
   </phase>
   <phase id="elgagab_section_LebensstilKodiert-20210219T092947-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947-closed"/>
   </phase>
   <phase id="elgagab_section_EmpfohleneMedikationUnkodiert-20210219T090548">
      <active pattern="template-1.2.40.0.34.6.0.11.2.56-2021-02-19T090548"/>
   </phase>
   <phase id="elgagab_section_EmpfohleneMedikationUnkodiert-20210219T090548-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.56-2021-02-19T090548-closed"/>
   </phase>
   <phase id="elgagab_section_diagnostikUndBefundeKodiert-20210219T104138">
      <active pattern="template-1.2.40.0.34.6.0.11.2.57-2021-02-19T104138"/>
   </phase>
   <phase id="elgagab_section_diagnostikUndBefundeKodiert-20210219T104138-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.57-2021-02-19T104138-closed"/>
   </phase>
   <phase id="elgagab_section_WeitereEmpfohleneMassnahmenKodiert-20210219T104329">
      <active pattern="template-1.2.40.0.34.6.0.11.2.58-2021-02-19T104329"/>
   </phase>
   <phase id="elgagab_section_WeitereEmpfohleneMassnahmenKodiert-20210219T104329-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.58-2021-02-19T104329-closed"/>
   </phase>
   <phase id="atcdabrr_section_AllergienUndIntoleranzenKodiert-20210219T114024">
      <active pattern="template-1.2.40.0.34.6.0.11.2.59-2021-02-19T114024"/>
   </phase>
   <phase id="atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert-20210219T115750">
      <active pattern="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750"/>
   </phase>
   <phase id="atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert-20210219T115750-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750-closed"/>
   </phase>
   <phase id="atcdabrr_section_WillenserklaerungenUndAndereJuridischeDokumente-20210219T120311">
      <active pattern="template-1.2.40.0.34.6.0.11.2.61-2021-02-19T120311"/>
   </phase>
   <phase id="atcdabrr_section_WillenserklaerungenUndAndereJuridischeDokumente-20210219T120311-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.61-2021-02-19T120311-closed"/>
   </phase>
   <phase id="atcdabrr_section_SUBWillenserklaerungenUndAndereJuridischeDokumente-20210219T115806">
      <active pattern="template-1.2.40.0.34.6.0.11.2.62-2021-02-19T115806"/>
   </phase>
   <phase id="atcdabrr_section_SUBWillenserklaerungenUndAndereJuridischeDokumente-20210219T115806-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.62-2021-02-19T115806-closed"/>
   </phase>
   <phase id="atcdabbr_section_MedikationslistePSKodiert-20210219T115029">
      <active pattern="template-1.2.40.0.34.6.0.11.2.63-2021-02-19T115029"/>
   </phase>
   <phase id="atcdabbr_section_MedikationslistePSKodiert-20210219T115029-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.63-2021-02-19T115029-closed"/>
   </phase>
   <phase id="elgagab_section_VerlaufSUBUnkodiert-20210219T104202">
      <active pattern="template-1.2.40.0.34.6.0.11.2.64-2021-02-19T104202"/>
   </phase>
   <phase id="elgagab_section_VerlaufSUBUnkodiert-20210219T104202-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.64-2021-02-19T104202-closed"/>
   </phase>
   <phase id="elgagab_section_VitalparameterUnkodiert-20210219T104313">
      <active pattern="template-1.2.40.0.34.6.0.11.2.68-2021-02-19T104313"/>
   </phase>
   <phase id="elgagab_section_VitalparameterUnkodiert-20210219T104313-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.68-2021-02-19T104313-closed"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed"/>
   </phase>
   <phase id="atcdabbr_section_AbschliessendeBemerkung-20210628T112503">
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503"/>
   </phase>
   <phase id="atcdabbr_section_AbschliessendeBemerkung-20210628T112503-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503-closed"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20210628T112240">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2021-06-28T112240"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20210628T112240-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2021-06-28T112240-closed"/>
   </phase>
   <phase id="atcdabrr_section_AusstehendeBefunde-20210219T114054">
      <active pattern="template-1.2.40.0.34.6.0.11.2.72-2021-02-19T114054"/>
   </phase>
   <phase id="atcdabrr_section_AusstehendeBefunde-20210219T114054-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.72-2021-02-19T114054-closed"/>
   </phase>
   <phase id="atcdabbr_section_EmpfohleneAnordnungenPflege-20210219T114423">
      <active pattern="template-1.2.40.0.34.6.0.11.2.73-2021-02-19T114423"/>
   </phase>
   <phase id="atcdabbr_section_EmpfohleneAnordnungenPflege-20210219T114423-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.73-2021-02-19T114423-closed"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenICD10Kodiert-20210219T090717">
      <active pattern="template-1.2.40.0.34.6.0.11.2.74-2021-02-19T090717"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenICD10Kodiert-20210219T090717-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.74-2021-02-19T090717-closed"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20210628T112805">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2021-06-28T112805"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20210628T112805-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2021-06-28T112805-closed"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseUnkodiert-20210223T062526">
      <active pattern="template-1.2.40.0.34.6.0.11.2.83-2021-02-23T062526"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseUnkodiert-20210223T062526-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.83-2021-02-23T062526-closed"/>
   </phase>
   <phase id="elgagab_section_ZusaetzlicheMedikationUnkodiert-20210219T104523">
      <active pattern="template-1.2.40.0.34.6.0.11.2.88-2021-02-19T104523"/>
   </phase>
   <phase id="elgagab_section_ZusaetzlicheMedikationUnkodiert-20210219T104523-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.88-2021-02-19T104523-closed"/>
   </phase>
   <phase id="elgagab_section_AenderungBestehenderMedikationKodiert-20210219T104533">
      <active pattern="template-1.2.40.0.34.6.0.11.2.89-2021-02-19T104533"/>
   </phase>
   <phase id="elgagab_section_AenderungBestehenderMedikationKodiert-20210219T104533-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.89-2021-02-19T104533-closed"/>
   </phase>
   <phase id="elgagab_section_AktuelleMedikationUnkodiert-20210219T084614">
      <active pattern="template-1.2.40.0.34.6.0.11.2.9-2021-02-19T084614"/>
   </phase>
   <phase id="elgagab_section_AktuelleMedikationUnkodiert-20210219T084614-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.9-2021-02-19T084614-closed"/>
   </phase>
   <phase id="elgagab_section_ZusaetzlicheMedikationKodiert-20210219T104512">
      <active pattern="template-1.2.40.0.34.6.0.11.2.90-2021-02-19T104512"/>
   </phase>
   <phase id="elgagab_section_ZusaetzlicheMedikationKodiert-20210219T104512-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.90-2021-02-19T104512-closed"/>
   </phase>
   <phase id="atcdabbr_section_DokumentierteEinnahmeKodiert-20210219T114416">
      <active pattern="template-1.2.40.0.34.6.0.11.2.94-2021-02-19T114416"/>
   </phase>
   <phase id="atcdabbr_section_DokumentierteEinnahmeKodiert-20210219T114416-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.94-2021-02-19T114416-closed"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseKodiert-20210223T062231">
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2021-02-23T062231"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseKodiert-20210223T062231-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2021-02-23T062231-closed"/>
   </phase>
   <phase id="atcdabbr_section_DokumentierteEinnahmeUnkodiert-20210219T090321">
      <active pattern="template-1.2.40.0.34.6.0.11.2.97-2021-02-19T090321"/>
   </phase>
   <phase id="atcdabbr_section_DokumentierteEinnahmeUnkodiert-20210219T090321-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.97-2021-02-19T090321-closed"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeAnamnese-20210219T090557">
      <active pattern="template-1.2.40.0.34.6.0.11.2.98-2021-02-19T090557"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeAnamnese-20210219T090557-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.98-2021-02-19T090557-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Immunization-20220125T142109">
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2022-01-25T142109"/>
   </phase>
   <phase id="atcdabbr_entry_Immunization-20220125T142109-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2022-01-25T142109-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationSchedule-20210804T144727">
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2021-08-04T144727"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationSchedule-20210804T144727-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2021-08-04T144727-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungVitalparameterEntry-20210128T145003">
      <active pattern="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungVitalparameterEntry-20210128T145003-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungsGruppeEntry-20210219T125910">
      <active pattern="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungsGruppeEntry-20210219T125910-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910-closed"/>
   </phase>
   <phase id="SerienmessungsWerteEntry-20210128T145841">
      <active pattern="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145841"/>
   </phase>
   <phase id="SerienmessungsWerteEntry-20210128T145841-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145841-closed"/>
   </phase>
   <phase id="SerienmessungsPeriodeEntry-20210219T125919">
      <active pattern="template-1.2.40.0.34.6.0.11.3.104-2021-02-19T125919"/>
   </phase>
   <phase id="SerienmessungsPeriodeEntry-20210219T125919-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.104-2021-02-19T125919-closed"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-20210219T124256">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-20210219T124256-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20210219T124340">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2021-02-19T124340"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20210219T124340-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2021-02-19T124340-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Zusatzklassifikation-20210519T150517">
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2021-05-19T150517"/>
   </phase>
   <phase id="atcdabbr_entry_Zusatzklassifikation-20210519T150517-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2021-05-19T150517-closed"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20210628T111327">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2021-06-28T111327"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20210628T111327-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2021-06-28T111327-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationTarget-20211028T134914">
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2021-10-28T134914"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationTarget-20211028T134914-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2021-10-28T134914-closed"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterGruppeEntry-20210219T130106">
      <active pattern="template-1.2.40.0.34.6.0.11.3.23-2021-02-19T130106"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterGruppeEntry-20210219T130106-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.23-2021-02-19T130106-closed"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterEntry-20210219T130055">
      <active pattern="template-1.2.40.0.34.6.0.11.3.24-2021-02-19T130055"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterEntry-20210219T130055-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.24-2021-02-19T130055-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationImpfungNichtAngegeben-20220310T141321">
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2022-03-10T141321"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationImpfungNichtAngegeben-20220310T141321-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2022-03-10T141321-closed"/>
   </phase>
   <phase id="elgagab_entry_Konsultationsgrund-20210219T104636">
      <active pattern="template-1.2.40.0.34.6.0.11.3.30-2021-02-19T104636"/>
   </phase>
   <phase id="elgagab_entry_Konsultationsgrund-20210219T104636-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.30-2021-02-19T104636-closed"/>
   </phase>
   <phase id="elgagab_entry_KonsultationsgrundProblem-20220602T152046">
      <active pattern="template-1.2.40.0.34.6.0.11.3.31-2023-02-02T154005"/>
   </phase>
   <phase id="elgagab_entry_KonsultationsgrundProblem-20220602T152046-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.31-2023-02-02T154005-closed"/>
   </phase>
   <phase id="atcdabbr_entry_AllergyOrIntoleranceConcern-20210219T124110">
      <active pattern="template-1.2.40.0.34.6.0.11.3.32-2021-02-19T124110"/>
   </phase>
   <phase id="atcdabbr_entry_AllergyOrIntolerance-20210219T123940">
      <active pattern="template-1.2.40.0.34.6.0.11.3.33-2021-02-19T123940"/>
   </phase>
   <phase id="atcdabbr_entry_ReactionManifestation-20210219T125610">
      <active pattern="template-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610"/>
   </phase>
   <phase id="atcdabbr_entry_ReactionManifestation-20210219T125610-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.34-2021-02-19T125610-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-20210628T134402">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-20210628T134402-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-20210219T124249">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-20210219T124249-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed"/>
   </phase>
   <phase id="atcdabbr_entry_AllergyStatusObservation-20210219T124116">
      <active pattern="template-1.2.40.0.34.6.0.11.3.37-2021-02-19T124116"/>
   </phase>
   <phase id="atcdabbr_entry_AllergyStatusObservation-20210219T124116-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.37-2021-02-19T124116-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-20210219T130038">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-20210219T130038-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MedicalDevice-20210219T125204">
      <active pattern="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204"/>
   </phase>
   <phase id="atcdabbr_entry_MedicalDevice-20210219T125204-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilTabakkonsumQuantitativ-20210219T125146">
      <active pattern="template-1.2.40.0.34.6.0.11.3.40-2021-02-19T125146"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilTabakkonsumQuantitativ-20210219T125146-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.40-2021-02-19T125146-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilTabakkonsumNominal-20210219T125138">
      <active pattern="template-1.2.40.0.34.6.0.11.3.41-2021-02-19T125138"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ-20210219T125132">
      <active pattern="template-1.2.40.0.34.6.0.11.3.42-2021-02-19T125132"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ-20210219T125132-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.42-2021-02-19T125132-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumNominal-20210219T125125">
      <active pattern="template-1.2.40.0.34.6.0.11.3.43-2021-02-19T125125"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumNominal-20210219T125125-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.43-2021-02-19T125125-closed"/>
   </phase>
   <phase id="atcdabbr_entry_AktuelleSchwangerschaft-20220314T112152">
      <active pattern="template-1.2.40.0.34.6.0.11.3.44-2022-03-14T112152"/>
   </phase>
   <phase id="atcdabbr_entry_AktuelleSchwangerschaft-20220314T112152-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.44-2022-03-14T112152-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ErwartetesGeburtsdatum-20210201T140251">
      <active pattern="template-1.2.40.0.34.6.0.11.3.45-2021-02-01T140251"/>
   </phase>
   <phase id="atcdabbr_entry_ErwartetesGeburtsdatum-20210201T140251-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.45-2021-02-01T140251-closed"/>
   </phase>
   <phase id="atcdabbr_entry_BisherigeSchwangerschaften-20210127T110027">
      <active pattern="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027"/>
   </phase>
   <phase id="atcdabbr_entry_BisherigeSchwangerschaften-20210127T110027-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027-closed"/>
   </phase>
   <phase id="atcdabbr_entry_FunctionalStatus-20210219T124346">
      <active pattern="template-1.2.40.0.34.6.0.11.3.47-2021-02-19T124346"/>
   </phase>
   <phase id="atcdabbr_entry_FunctionalStatus-20210219T124346-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.47-2021-02-19T124346-closed"/>
   </phase>
   <phase id="ELGA_HistoryOfProcedures-20210219T104625">
      <active pattern="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536"/>
   </phase>
   <phase id="ELGA_HistoryOfProcedures-20210219T104625-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-20210219T125553">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-20210219T125553-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationBillability-20210219T124642">
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2021-02-19T124642"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationBillability-20210219T124642-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2021-02-19T124642-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MedicationStatement-20210219T125214">
      <active pattern="template-1.2.40.0.34.6.0.11.3.50-2021-02-19T125214"/>
   </phase>
   <phase id="atcdabbr_entry_MedicationStatement-20210219T125214-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.50-2021-02-19T125214-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Procedure-20210219T125601">
      <active pattern="template-1.2.40.0.34.6.0.11.3.51-2023-02-03T103752"/>
   </phase>
   <phase id="atcdabbr_entry_Procedure-20210219T125601-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.51-2023-02-03T103752-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Problem-20220602T151036">
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045"/>
   </phase>
   <phase id="atcdabbr_entry_Problem-20220602T151036-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemConcern-20210219T125533">
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemConcern-20210219T125533-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MedikationsEinnahmeEntry-20210219T125222">
      <active pattern="template-1.2.40.0.34.6.0.11.3.72-2021-02-19T125222"/>
   </phase>
   <phase id="atcdabbr_entry_MedikationsEinnahmeEntry-20210219T125222-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.72-2021-02-19T125222-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyTranscriber-20210804T154113">
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyTranscriber-20210804T154113-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyImpfendePerson-20211013T125337">
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyImpfendePerson-20211013T125337-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337-closed"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProductNichtAngegeben-20210219T133513">
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProductNichtAngegeben-20210219T133513-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513-closed"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProduct-20220712T085429">
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2022-07-12T085429"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProduct-20220712T085429-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2022-07-12T085429-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyVerifier">
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2021-08-04T122820"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyVerifier-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2021-08-04T122820-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyAuthorizedEditor">
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyAuthorizedEditor-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2021-12-03T123540-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyDataEnterer">
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2022-01-25T134202"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyDataEnterer-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2022-01-25T134202-closed"/>
   </phase>

   <!-- Include schematrons from templates with explicit * or ** context (but no representing templates), only those used in scenario template -->

   <!-- EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.11.1.3.1-2017-05-05T000000.sch"/>
   <!-- ProblemStatusObservationEntry -->
   <include href="include/1.2.40.0.34.11.13.3.13-2017-07-13T210449.sch"/>
   <!-- CriticalityObservation -->
   <include href="include/1.2.40.0.34.11.13.3.18-2017-08-10T195216.sch"/>
   <!-- CertaintyObservation -->
   <include href="include/1.2.40.0.34.11.13.3.19-2017-08-10T202554.sch"/>
   <!-- SeverityObservation -->
   <include href="include/1.2.40.0.34.11.13.3.21-2017-08-20T120835.sch"/>
   <!-- GesundheitsproblemBedenkenEntry -->
   <include href="include/1.2.40.0.34.11.13.3.6-2017-01-26T142954.sch"/>
   <!-- ProblemEntryGesundheitsproblem -->
   <include href="include/1.2.40.0.34.11.13.3.7-2017-08-13T150852.sch"/>
   <!-- MedikationVerordnungEntryemed -->
   <include href="include/1.2.40.0.34.11.8.1.3.1-2014-09-01T000000.sch"/>
   <!-- MedikationAbgabeEntryemed -->
   <include href="include/1.2.40.0.34.11.8.2.3.1-2014-09-10T000000.sch"/>
   <!-- MedikationPharmazeutischeEmpfehlungEntry -->
   <include href="include/1.2.40.0.34.11.8.4.3.1-2014-03-04T000000.sch"/>
   <!-- atcdabrr_section_ImpfungenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.1-2021-02-19T114643.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.1-2021-02-19T114643-closed.sch"/>
   <!-- elgagab_section_anamnese -->
   <include href="include/1.2.40.0.34.6.0.11.2.10-2021-02-19T084636.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.10-2021-02-19T084636-closed.sch"/>
   <!-- elgagab_section_diagnostikUndBefundeUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.11-2021-02-19T104146.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.11-2021-02-19T104146-closed.sch"/>
   <!-- elgagab_section_Verlauf -->
   <include href="include/1.2.40.0.34.6.0.11.2.12-2021-02-19T104156.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.12-2021-02-19T104156-closed.sch"/>
   <!-- elgagab_section_durchgefuehrteMassnahmenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.13-2021-02-19T090307.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.13-2021-02-19T090307-closed.sch"/>
   <!-- elgagab_section_pflegemassnahmen -->
   <include href="include/1.2.40.0.34.6.0.11.2.14-2021-02-19T103551.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.14-2021-02-19T103551-closed.sch"/>
   <!-- elgagab_section_FruehereErkrankungenMassnahmenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.16-2021-02-19T090653.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.16-2021-02-19T090653-closed.sch"/>
   <!-- elgagab_section_KonsultUeberweisungsgrundUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.17-2021-02-19T092953.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.17-2021-02-19T092953-closed.sch"/>
   <!-- elgagab_section_FachspezifischeDiagnostik -->
   <include href="include/1.2.40.0.34.6.0.11.2.19-2021-02-19T090603.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.19-2021-02-19T090603-closed.sch"/>
   <!-- elgagab_section_Status -->
   <include href="include/1.2.40.0.34.6.0.11.2.20-2021-02-19T103545.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.20-2021-02-19T103545-closed.sch"/>
   <!-- elgagab_section_EmpfohleneMedikationKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.21-2021-02-19T090540.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.21-2021-02-19T090540-closed.sch"/>
   <!-- elgagab_section_durchgefuehrteMassnahmenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.22-2021-02-19T090241.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.22-2021-02-19T090241-closed.sch"/>
   <!-- elgagab_section_weitereEmpfohleneMassnahmenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.23-2021-02-19T104339.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.23-2021-02-19T104339-closed.sch"/>
   <!-- elgagab_section_ZusammenfassendeBeurteilung -->
   <include href="include/1.2.40.0.34.6.0.11.2.25-2021-02-19T104512.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.25-2021-02-19T104512-closed.sch"/>
   <!-- elgagab_section_WeitereInformationen -->
   <include href="include/1.2.40.0.34.6.0.11.2.26-2021-02-19T104355.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.26-2021-02-19T104355-closed.sch"/>
   <!-- elgagab_section_allergienIntoleranzenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.41-2021-02-19T084624.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.41-2021-02-19T084624-closed.sch"/>
   <!-- elgagab_section_TermineKontrollenWiederbestellungen -->
   <include href="include/1.2.40.0.34.6.0.11.2.42-2021-02-19T104151.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.42-2021-02-19T104151-closed.sch"/>
   <!-- elgagab_section_geplanteUntersuchungen -->
   <include href="include/1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.43-2021-02-19T090640-closed.sch"/>
   <!-- elgagab_section_konservativeTherapie -->
   <include href="include/1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.44-2021-02-19T093018-closed.sch"/>
   <!-- elgagab_section_chirurgischeTherapie -->
   <include href="include/1.2.40.0.34.6.0.11.2.45-2021-02-19T090340.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.45-2021-02-19T090340-closed.sch"/>
   <!-- atcdabrr_section_VitalparameterKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.46-2021-02-19T120303.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.46-2021-02-19T120303-closed.sch"/>
   <!-- elgagab_section_KonsultUeberweisungsgrundKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.47-2021-02-19T093000.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.47-2021-02-19T093000-closed.sch"/>
   <!-- atcdabrr_section_SchwangerschaftenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.48-2021-01-28T144037-closed.sch"/>
   <!-- elgagab_section_SchwangerschaftenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.49-2021-02-19T103539.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.49-2021-02-19T103539-closed.sch"/>
   <!-- elgagab_section_ImplantateUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.50-2021-02-19T103530.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.50-2021-02-19T103530-closed.sch"/>
   <!-- cdagab_section_BeeintraechtigungenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.51-2021-02-19T090357.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.51-2021-02-19T090357-closed.sch"/>
   <!-- elgagab_section_BeeintraechtigungenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.52-2021-02-19T090348.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.52-2021-02-19T090348-closed.sch"/>
   <!-- elgagab_section_ImpfungenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.53-2021-02-19T093025.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.53-2021-02-19T093025-closed.sch"/>
   <!-- elgagab_section_LebensstilUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.54-2021-02-19T092815.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.54-2021-02-19T092815-closed.sch"/>
   <!-- elgagab_section_LebensstilKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.55-2021-02-19T092947-closed.sch"/>
   <!-- elgagab_section_EmpfohleneMedikationUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.56-2021-02-19T090548.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.56-2021-02-19T090548-closed.sch"/>
   <!-- elgagab_section_diagnostikUndBefundeKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.57-2021-02-19T104138.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.57-2021-02-19T104138-closed.sch"/>
   <!-- elgagab_section_WeitereEmpfohleneMassnahmenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.58-2021-02-19T104329.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.58-2021-02-19T104329-closed.sch"/>
   <!-- atcdabrr_section_AllergienUndIntoleranzenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.59-2021-02-19T114024.sch"/>
   <!-- atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.60-2021-02-19T115750-closed.sch"/>
   <!-- atcdabrr_section_WillenserklaerungenUndAndereJuridischeDokumente -->
   <include href="include/1.2.40.0.34.6.0.11.2.61-2021-02-19T120311.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.61-2021-02-19T120311-closed.sch"/>
   <!-- atcdabrr_section_SUBWillenserklaerungenUndAndereJuridischeDokumente -->
   <include href="include/1.2.40.0.34.6.0.11.2.62-2021-02-19T115806.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.62-2021-02-19T115806-closed.sch"/>
   <!-- atcdabbr_section_MedikationslistePSKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.63-2021-02-19T115029.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.63-2021-02-19T115029-closed.sch"/>
   <!-- elgagab_section_VerlaufSUBUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.64-2021-02-19T104202.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.64-2021-02-19T104202-closed.sch"/>
   <!-- elgagab_section_VitalparameterUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.68-2021-02-19T104313.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.68-2021-02-19T104313-closed.sch"/>
   <!-- atcdabbr_section_Brieftext -->
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed.sch"/>
   <!-- atcdabbr_section_AbschliessendeBemerkung -->
   <include href="include/1.2.40.0.34.6.0.11.2.70-2021-06-28T112503.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.70-2021-06-28T112503-closed.sch"/>
   <!-- atcdabbr_section_Beilagen -->
   <include href="include/1.2.40.0.34.6.0.11.2.71-2021-06-28T112240.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.71-2021-06-28T112240-closed.sch"/>
   <!-- atcdabrr_section_AusstehendeBefunde -->
   <include href="include/1.2.40.0.34.6.0.11.2.72-2021-02-19T114054.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.72-2021-02-19T114054-closed.sch"/>
   <!-- atcdabbr_section_EmpfohleneAnordnungenPflege -->
   <include href="include/1.2.40.0.34.6.0.11.2.73-2021-02-19T114423.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.73-2021-02-19T114423-closed.sch"/>
   <!-- elgagab_section_FruehereErkrankungenMassnahmenICD10Kodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.74-2021-02-19T090717.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.74-2021-02-19T090717-closed.sch"/>
   <!-- atcdabbr_section_Uebersetzung -->
   <include href="include/1.2.40.0.34.6.0.11.2.8-2021-06-28T112805.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.8-2021-06-28T112805-closed.sch"/>
   <!-- atcdabbr_section_DiagnoseUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.83-2021-02-23T062526.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.83-2021-02-23T062526-closed.sch"/>
   <!-- elgagab_section_ZusaetzlicheMedikationUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.88-2021-02-19T104523.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.88-2021-02-19T104523-closed.sch"/>
   <!-- elgagab_section_AenderungBestehenderMedikationKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.89-2021-02-19T104533.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.89-2021-02-19T104533-closed.sch"/>
   <!-- elgagab_section_AktuelleMedikationUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.9-2021-02-19T084614.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.9-2021-02-19T084614-closed.sch"/>
   <!-- elgagab_section_ZusaetzlicheMedikationKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.90-2021-02-19T104512.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.90-2021-02-19T104512-closed.sch"/>
   <!-- atcdabbr_section_DokumentierteEinnahmeKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.94-2021-02-19T114416.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.94-2021-02-19T114416-closed.sch"/>
   <!-- atcdabbr_section_DiagnoseKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.96-2021-02-23T062231.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.96-2021-02-23T062231-closed.sch"/>
   <!-- atcdabbr_section_DokumentierteEinnahmeUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.97-2021-02-19T090321.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.97-2021-02-19T090321-closed.sch"/>
   <!-- elgagab_section_FachspezifischeAnamnese -->
   <include href="include/1.2.40.0.34.6.0.11.2.98-2021-02-19T090557.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.98-2021-02-19T090557-closed.sch"/>
   <!-- atcdabbr_entry_Immunization -->
   <include href="include/1.2.40.0.34.6.0.11.3.1-2022-01-25T142109.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.1-2022-01-25T142109-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationSchedule -->
   <include href="include/1.2.40.0.34.6.0.11.3.10-2021-08-04T144727.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.10-2021-08-04T144727-closed.sch"/>
   <!-- atcdabbr_entry_SerienmessungVitalparameterEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.100-2021-01-28T145003-closed.sch"/>
   <!-- atcdabbr_entry_SerienmessungsGruppeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.102-2021-02-19T125910-closed.sch"/>
   <!-- SerienmessungsWerteEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.103-2021-01-28T145841.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.103-2021-01-28T145841-closed.sch"/>
   <!-- SerienmessungsPeriodeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.104-2021-02-19T125919.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.104-2021-02-19T125919-closed.sch"/>
   <!-- atcdabrr_entry_Comment -->
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed.sch"/>
   <!-- atcdabbr_entry_externalDocument -->
   <include href="include/1.2.40.0.34.6.0.11.3.14-2021-02-19T124340.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.14-2021-02-19T124340-closed.sch"/>
   <!-- atcdabbr_entry_Zusatzklassifikation -->
   <include href="include/1.2.40.0.34.6.0.11.3.168-2021-05-19T150517.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.168-2021-05-19T150517-closed.sch"/>
   <!-- atcdabbr_entry_EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.19-2021-06-28T111327.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.19-2021-06-28T111327-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationTarget -->
   <include href="include/1.2.40.0.34.6.0.11.3.2-2021-10-28T134914.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.2-2021-10-28T134914-closed.sch"/>
   <!-- atcdabbr_entry_VitalparameterGruppeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.23-2021-02-19T130106.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.23-2021-02-19T130106-closed.sch"/>
   <!-- atcdabbr_entry_VitalparameterEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.24-2021-02-19T130055.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.24-2021-02-19T130055-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationImpfungNichtAngegeben -->
   <include href="include/1.2.40.0.34.6.0.11.3.28-2022-03-10T141321.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.28-2022-03-10T141321-closed.sch"/>
   <!-- elgagab_entry_Konsultationsgrund -->
   <include href="include/1.2.40.0.34.6.0.11.3.30-2021-02-19T104636.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.30-2021-02-19T104636-closed.sch"/>
   <!-- elgagab_entry_KonsultationsgrundProblem -->
   <include href="include/1.2.40.0.34.6.0.11.3.31-2023-02-02T154005.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.31-2023-02-02T154005-closed.sch"/>
   <!-- atcdabbr_entry_AllergyOrIntoleranceConcern -->
   <include href="include/1.2.40.0.34.6.0.11.3.32-2021-02-19T124110.sch"/>
   <!-- atcdabbr_entry_AllergyOrIntolerance -->
   <include href="include/1.2.40.0.34.6.0.11.3.33-2021-02-19T123940.sch"/>
   <!-- atcdabbr_entry_ReactionManifestation -->
   <include href="include/1.2.40.0.34.6.0.11.3.34-2021-02-19T125610.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.34-2021-02-19T125610-closed.sch"/>
   <!-- atcdabbr_entry_CriticalityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed.sch"/>
   <!-- atcdabbr_entry_CertaintyObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed.sch"/>
   <!-- atcdabbr_entry_AllergyStatusObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.37-2021-02-19T124116.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.37-2021-02-19T124116-closed.sch"/>
   <!-- atcdabbr_entry_SeverityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed.sch"/>
   <!-- atcdabbr_entry_MedicalDevice -->
   <include href="include/1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.39-2021-02-19T125204-closed.sch"/>
   <!-- atcdabbr_entry_LebensstilTabakkonsumQuantitativ -->
   <include href="include/1.2.40.0.34.6.0.11.3.40-2021-02-19T125146.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.40-2021-02-19T125146-closed.sch"/>
   <!-- atcdabbr_entry_LebensstilTabakkonsumNominal -->
   <include href="include/1.2.40.0.34.6.0.11.3.41-2021-02-19T125138.sch"/>
   <!-- atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ -->
   <include href="include/1.2.40.0.34.6.0.11.3.42-2021-02-19T125132.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.42-2021-02-19T125132-closed.sch"/>
   <!-- atcdabbr_entry_LebensstilAlkoholkonsumNominal -->
   <include href="include/1.2.40.0.34.6.0.11.3.43-2021-02-19T125125.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.43-2021-02-19T125125-closed.sch"/>
   <!-- atcdabbr_entry_AktuelleSchwangerschaft -->
   <include href="include/1.2.40.0.34.6.0.11.3.44-2022-03-14T112152.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.44-2022-03-14T112152-closed.sch"/>
   <!-- atcdabbr_entry_ErwartetesGeburtsdatum -->
   <include href="include/1.2.40.0.34.6.0.11.3.45-2021-02-01T140251.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.45-2021-02-01T140251-closed.sch"/>
   <!-- atcdabbr_entry_BisherigeSchwangerschaften -->
   <include href="include/1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.46-2021-01-27T110027-closed.sch"/>
   <!-- atcdabbr_entry_FunctionalStatus -->
   <include href="include/1.2.40.0.34.6.0.11.3.47-2021-02-19T124346.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.47-2021-02-19T124346-closed.sch"/>
   <!-- ELGA_HistoryOfProcedures -->
   <include href="include/1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.48-2023-02-03T102536-closed.sch"/>
   <!-- atcdabbr_entry_ProblemStatusObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationBillability -->
   <include href="include/1.2.40.0.34.6.0.11.3.5-2021-02-19T124642.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.5-2021-02-19T124642-closed.sch"/>
   <!-- atcdabbr_entry_MedicationStatement -->
   <include href="include/1.2.40.0.34.6.0.11.3.50-2021-02-19T125214.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.50-2021-02-19T125214-closed.sch"/>
   <!-- atcdabbr_entry_Procedure -->
   <include href="include/1.2.40.0.34.6.0.11.3.51-2023-02-03T103752.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.51-2023-02-03T103752-closed.sch"/>
   <!-- atcdabbr_entry_Logo -->
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed.sch"/>
   <!-- atcdabbr_entry_Problem -->
   <include href="include/1.2.40.0.34.6.0.11.3.6-2023-02-02T155045.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.6-2023-02-02T155045-closed.sch"/>
   <!-- atcdabbr_entry_ProblemConcern -->
   <include href="include/1.2.40.0.34.6.0.11.3.7-2021-02-19T125533.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.7-2021-02-19T125533-closed.sch"/>
   <!-- atcdabbr_entry_MedikationsEinnahmeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.72-2021-02-19T125222.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.72-2021-02-19T125222-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyTranscriber -->
   <include href="include/1.2.40.0.34.6.0.11.9.14-2021-08-04T154113.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.14-2021-08-04T154113-closed.sch"/>
   <!-- atcdabbr_other_PerformerBody -->
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed.sch"/>
   <!-- atcdabbr_other_PerformerBodyImpfendePerson -->
   <include href="include/1.2.40.0.34.6.0.11.9.21-2021-10-13T125337.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.21-2021-10-13T125337-closed.sch"/>
   <!-- atcdabbr_other_vaccineProductNichtAngegeben -->
   <include href="include/1.2.40.0.34.6.0.11.9.31-2021-02-19T133513.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.31-2021-02-19T133513-closed.sch"/>
   <!-- atcdabbr_other_vaccineProduct -->
   <include href="include/1.2.40.0.34.6.0.11.9.32-2022-07-12T085429.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.32-2022-07-12T085429-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyVerifier -->
   <include href="include/1.2.40.0.34.6.0.11.9.44-2021-08-04T122820.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.44-2021-08-04T122820-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyAuthorizedEditor -->
   <include href="include/1.2.40.0.34.6.0.11.9.46-2021-12-03T123540.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.46-2021-12-03T123540-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyDataEnterer -->
   <include href="include/1.2.40.0.34.6.0.11.9.47-2022-01-25T134202.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.47-2022-01-25T134202-closed.sch"/>

</schema>
